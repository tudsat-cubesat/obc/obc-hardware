Specifications

# Original


* STM32-Microcontroller
    * 2 CAN-Controllers
    * 2 CAN-Transceiver
    * 5 Pin CAN connector    
* External crystal (~16MHZ)
* 5 status LEDs (different colors)
* Analog (De-)Multiplexer (>=12 channels)
    * Pins exposed
* Shiftregister (>= 8 channels)
    * Pins exposed
* SWD/ Interface
* EEPROM/FRAM (with same interface and pinout) 
    * 2 if possible
    * Interface exposed
* Area for experiments 
* PC104
* Power plug 
    * Barel jack 
    * Pins (polarity safe)
    * LED
    * Fuse optional
* Board temp sensors
    * 1 - 4


# V1 - ___
* STM32-Microcontroller (STM32L496VGT3)
    * 2 CAN-Controllers
    * 2 CAN-Transceiver ()
    * 5 Pin CAN connector    
* External crystal (24MHZ)
* 4 status LEDs (different colors)
* Analog (De-)Multiplexer - 16 channels
    * Pins exposed as 1x16 Pin connector
        * PCDU compatible 
* SWD/ Interface
* External memory
    * Interface exposed
    * SPI and select pins of each module
    * EEPROM/FRAM (same interface and pinout) 
        * 9 modules
    * 1 MicroSD-Cards 
* Area for experiments 
* PC104 formfactor
* Power plug 
    * USB 
    * Pins (polarity safe)
    * Status LED
* Board temp sensors
    * 1 (+ Chip intern)
