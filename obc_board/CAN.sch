EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 10
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L obc_board-rescue:SN65HVD233-Interface_CAN_LIN CAN1
U 1 1 604C7750
P 4900 3150
F 0 "CAN1" H 5200 2800 50  0000 C CNN
F 1 "SN65HVD230" H 4550 2800 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 4900 2650 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/sn65hvd234.pdf" H 4800 3550 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/texas-instruments/SN65HVD230DR/296-11654-1-ND/404366" H 4900 3150 50  0001 C CNN "Digikey"
F 5 "1,85" H 4900 3150 50  0001 C CNN "Cost"
F 6 "Ignore" H 4900 3150 50  0001 C CNN "PN"
	1    4900 3150
	1    0    0    -1  
$EndComp
Text GLabel 5550 3050 2    50   Input ~ 0
CAN_1_H
Text GLabel 5550 3350 2    50   Input ~ 0
CAN_1_L
Text GLabel 4500 3050 0    50   Input ~ 0
CAN_1_TX
Text GLabel 4500 3150 0    50   Input ~ 0
CAN_1_RX
$Comp
L obc_board-rescue:R-Device R1
U 1 1 604C775D
P 5400 3200
F 0 "R1" V 5400 3150 50  0000 L CNN
F 1 "120" V 5500 3100 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 5330 3200 50  0001 C CNN
F 3 "https://www.rohm.com/datasheet/ESR01MZPF/esr-e" H 5400 3200 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/rohm-semiconductor/ESR03EZPJ121/RHM120DCT-ND/1983778" V 5400 3200 50  0001 C CNN "Digikey"
F 5 "0,08" V 5400 3200 50  0001 C CNN "Cost"
F 6 "R" H 5400 3200 50  0001 C CNN "PN"
	1    5400 3200
	1    0    0    -1  
$EndComp
Text GLabel 4500 4200 0    50   Input ~ 0
CAN_2_RX
Text GLabel 4500 4100 0    50   Input ~ 0
CAN_2_TX
$Comp
L obc_board-rescue:SN65HVD233-Interface_CAN_LIN CAN2
U 1 1 604C7768
P 4900 4200
F 0 "CAN2" H 5200 3850 50  0000 C CNN
F 1 "SN65HVD230" H 4550 3850 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 4900 3700 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/sn65hvd234.pdf" H 4800 4600 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/texas-instruments/SN65HVD230DR/296-11654-1-ND/404366" H 4900 4200 50  0001 C CNN "Digikey"
F 5 "1,85" H 4900 4200 50  0001 C CNN "Cost"
F 6 "Ignore" H 4900 4200 50  0001 C CNN "PN"
	1    4900 4200
	1    0    0    -1  
$EndComp
Text GLabel 5550 4400 2    50   Input ~ 0
CAN_2_L
Text GLabel 5550 4100 2    50   Input ~ 0
CAN_2_H
Text GLabel 4900 3550 2    50   Input ~ 0
GND
Wire Wire Line
	4500 3550 4500 3350
Text GLabel 5100 2850 2    50   Input ~ 0
VDD_CAN
Wire Wire Line
	4500 4600 4500 4400
Text GLabel 4900 4600 2    50   Input ~ 0
GND
Text GLabel 5100 3900 2    50   Input ~ 0
VDD_CAN
Text Notes 4250 2700 0    50   ~ 0
CAN
Wire Notes Line
	4050 2600 4050 4750
Wire Notes Line
	6450 4750 6450 2600
Wire Wire Line
	4500 3550 4900 3550
Wire Wire Line
	4500 4600 4900 4600
Text GLabel 4700 2850 0    50   Input ~ 0
GND
Text GLabel 4700 3900 0    50   Input ~ 0
GND
NoConn ~ 4500 3250
NoConn ~ 4500 4300
Wire Wire Line
	5550 3050 5400 3050
Wire Wire Line
	5300 3050 5300 3150
Connection ~ 5400 3050
Wire Wire Line
	5400 3050 5300 3050
Wire Wire Line
	5550 3350 5400 3350
Wire Wire Line
	5300 3350 5300 3250
Connection ~ 5400 3350
Wire Wire Line
	5400 3350 5300 3350
Wire Wire Line
	5300 4100 5300 4200
Wire Wire Line
	5300 4400 5300 4300
Wire Notes Line
	4050 4750 6450 4750
Wire Notes Line
	4050 2600 6450 2600
$Comp
L obc_board-rescue:Ferrite_Bead_Small-Device FB1
U 1 1 604C778E
P 5000 2850
F 0 "FB1" V 5100 2750 50  0000 C CNN
F 1 "1k/600" V 5150 3000 50  0001 C CNN
F 2 "Inductor_SMD:L_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 4930 2850 50  0001 C CNN
F 3 "https://www.we-online.de/katalog/datasheet/742792662.pdf" H 5000 2850 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/w%C3%BCrth-elektronik/742792662/732-4670-1-ND/4310442" V 5000 2850 50  0001 C CNN "Digikey"
F 5 "0,14" V 5000 2850 50  0001 C CNN "Cost"
F 6 "Ignore" H 5000 2850 50  0001 C CNN "PN"
	1    5000 2850
	0    -1   -1   0   
$EndComp
$Comp
L obc_board-rescue:Ferrite_Bead_Small-Device FB2
U 1 1 604C7797
P 5000 3900
F 0 "FB2" V 5100 3800 50  0000 C CNN
F 1 "1k/600" V 5150 4050 50  0001 C CNN
F 2 "Inductor_SMD:L_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 4930 3900 50  0001 C CNN
F 3 "https://www.we-online.de/katalog/datasheet/742792662.pdf" H 5000 3900 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/w%C3%BCrth-elektronik/742792662/732-4670-1-ND/4310442" V 5000 3900 50  0001 C CNN "Digikey"
F 5 "0,14" V 5000 3900 50  0001 C CNN "Cost"
F 6 "Ignore" H 5000 3900 50  0001 C CNN "PN"
	1    5000 3900
	0    -1   -1   0   
$EndComp
$Comp
L obc_board-rescue:R-Device R2
U 1 1 604C77A0
P 5400 4250
F 0 "R2" V 5400 4200 50  0000 L CNN
F 1 "120" V 5500 4150 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 5330 4250 50  0001 C CNN
F 3 "https://www.rohm.com/datasheet/ESR01MZPF/esr-e" H 5400 4250 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/rohm-semiconductor/ESR03EZPJ121/RHM120DCT-ND/1983778" V 5400 4250 50  0001 C CNN "Digikey"
F 5 "0,08" V 5400 4250 50  0001 C CNN "Cost"
F 6 "R" H 5400 4250 50  0001 C CNN "PN"
	1    5400 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	5300 4100 5400 4100
Wire Wire Line
	5300 4400 5400 4400
Connection ~ 5400 4100
Wire Wire Line
	5400 4100 5550 4100
Connection ~ 5400 4400
Wire Wire Line
	5400 4400 5550 4400
$Comp
L obc_board-rescue:C_Small-Device C1
U 1 1 604C77AF
P 4800 2850
F 0 "C1" V 4900 2900 50  0000 L CNN
F 1 "100n" V 4900 2650 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 4800 2850 50  0001 C CNN
F 3 "https://www.yageo.com/upload/media/product/productsearch/datasheet/mlcc/UPY-GPHC_X7R_6.3V-to-50V_18.pdf" H 4800 2850 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/yageo/CC0603KPX7R7BB104/311-1335-1-ND/2103119" V 4800 2850 50  0001 C CNN "Digikey"
F 5 "0,08" V 4800 2850 50  0001 C CNN "Cost"
F 6 "C" H 4800 2850 50  0001 C CNN "PN"
	1    4800 2850
	0    -1   -1   0   
$EndComp
$Comp
L obc_board-rescue:C_Small-Device C2
U 1 1 604C77B8
P 4800 3900
F 0 "C2" V 4900 3950 50  0000 L CNN
F 1 "100n" V 4900 3700 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 4800 3900 50  0001 C CNN
F 3 "https://www.yageo.com/upload/media/product/productsearch/datasheet/mlcc/UPY-GPHC_X7R_6.3V-to-50V_18.pdf" H 4800 3900 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/yageo/CC0603KPX7R7BB104/311-1335-1-ND/2103119" V 4800 3900 50  0001 C CNN "Digikey"
F 5 "0,08" V 4800 3900 50  0001 C CNN "Cost"
F 6 "C" H 4800 3900 50  0001 C CNN "PN"
	1    4800 3900
	0    -1   -1   0   
$EndComp
$Comp
L obc_board-rescue:SolderJumper_2_Open-Jumper JP8
U 1 1 604C77BF
P 5900 3700
F 0 "JP8" H 5900 3905 50  0000 C CNN
F 1 "SolderJumper_2_Open" H 5900 3814 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_TrianglePad1.0x1.5mm" H 5900 3700 50  0001 C CNN
F 3 "~" H 5900 3700 50  0001 C CNN
F 4 "Ignore" H 5900 3700 50  0001 C CNN "PN"
	1    5900 3700
	1    0    0    -1  
$EndComp
Text GLabel 6150 3700 2    50   Input ~ 0
VDD
Text GLabel 5650 3700 0    50   Input ~ 0
VDD_CAN
Wire Wire Line
	5650 3700 5750 3700
Wire Wire Line
	6050 3700 6150 3700
$EndSCHEMATC
