EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 7 10
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text GLabel 3150 3850 2    50   Input ~ 0
PermMem1Select
Text GLabel 2350 3950 0    50   Input ~ 0
PermMemSDI
Text GLabel 2350 3850 0    50   Input ~ 0
PermMemSDO
Text GLabel 2350 4050 0    50   Input ~ 0
PermMemSClock
Text GLabel 2350 3350 0    50   Input ~ 0
PermMemSClock
Text GLabel 2350 3150 0    50   Input ~ 0
PermMemSDO
Text GLabel 2350 3250 0    50   Input ~ 0
PermMemSDI
Text GLabel 3150 3150 2    50   Input ~ 0
PermMem0Select
Text GLabel 5550 3150 2    50   Input ~ 0
PermMem3Select
Text GLabel 4750 3250 0    50   Input ~ 0
PermMemSDI
Text GLabel 4750 3150 0    50   Input ~ 0
PermMemSDO
Text GLabel 4750 3350 0    50   Input ~ 0
PermMemSClock
$Comp
L obc_board-rescue:M95256-WMN6P-Memory_EEPROM PermMem?
U 1 1 604BA22B
P 5150 3250
AR Path="/604BA22B" Ref="PermMem?"  Part="1" 
AR Path="/6026EB4B/604BA22B" Ref="PermMem3"  Part="1" 
F 0 "PermMem3" H 5800 3050 50  0000 C CNN
F 1 "CAT25256" H 5600 3000 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 5150 3250 50  0001 C CNN
F 3 "https://www.onsemi.com/pdf/datasheet/cat25256-d.pdf" H 5150 3250 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/on-semiconductor/CAT25256VI-GT3/CAT25256VI-GT3OSCT-ND/2704984" H 5150 3250 50  0001 C CNN "Digikey"
F 5 "2,20" H 5150 3250 50  0001 C CNN "Cost"
F 6 "Ignore" H 5150 3250 50  0001 C CNN "PN"
	1    5150 3250
	-1   0    0    1   
$EndComp
Text GLabel 3150 4550 2    50   Input ~ 0
PermMem2Select
Text GLabel 2800 2850 2    50   Input ~ 0
SPI_1_SCK
Text GLabel 2800 2750 2    50   Input ~ 0
SPI_1_MISO
Text GLabel 2800 2650 2    50   Input ~ 0
SPI_1_MOSI
Text GLabel 2750 2650 0    50   Input ~ 0
PermMemSDI
Text GLabel 2750 2750 0    50   Input ~ 0
PermMemSDO
Text GLabel 2750 2850 0    50   Input ~ 0
PermMemSClock
Text GLabel 4200 2600 2    50   Input ~ 0
PermMem0Select
Text GLabel 4200 2700 2    50   Input ~ 0
PermMem1Select
Text GLabel 4200 2800 2    50   Input ~ 0
PermMem2Select
Text GLabel 4200 2900 2    50   Input ~ 0
PermMem3Select
Text GLabel 4100 2700 0    50   Input ~ 0
SPI_1_CS_1
Text GLabel 4100 2800 0    50   Input ~ 0
SPI_1_CS_2
Text GLabel 4100 2900 0    50   Input ~ 0
SPI_1_CS_3
Text GLabel 4100 2600 0    50   Input ~ 0
SPI_1_CS_0
Wire Wire Line
	4200 2600 4100 2600
Wire Wire Line
	4100 2700 4200 2700
Wire Wire Line
	4200 2800 4100 2800
Wire Wire Line
	4100 2900 4200 2900
Wire Wire Line
	2800 2650 2750 2650
Wire Wire Line
	2750 2750 2800 2750
Wire Wire Line
	2800 2850 2750 2850
Text GLabel 2750 2950 2    50   Input ~ 0
GND
Text GLabel 2750 3550 2    50   Input ~ 0
VDD_FLASH
Text GLabel 2750 3650 2    50   Input ~ 0
GND
Text GLabel 5150 2950 2    50   Input ~ 0
GND
Text GLabel 2750 4250 2    50   Input ~ 0
VDD_FLASH
Text GLabel 2750 4950 2    50   Input ~ 0
VDD_FLASH
Text GLabel 5150 3550 2    50   Input ~ 0
VDD_FLASH
$Comp
L obc_board-rescue:M95256-WMN6P-Memory_EEPROM PermMem?
U 1 1 604BA253
P 2750 3950
AR Path="/604BA253" Ref="PermMem?"  Part="1" 
AR Path="/6026EB4B/604BA253" Ref="PermMem1"  Part="1" 
F 0 "PermMem1" H 3450 3750 50  0000 C CNN
F 1 "CAT25256" H 3200 3700 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 2750 3950 50  0001 C CNN
F 3 "https://www.onsemi.com/pdf/datasheet/cat25256-d.pdf" H 2750 3950 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/on-semiconductor/CAT25256VI-GT3/CAT25256VI-GT3OSCT-ND/2704984" H 2750 3950 50  0001 C CNN "Digikey"
F 5 "2,20" H 2750 3950 50  0001 C CNN "Cost"
F 6 "Ignore" H 2750 3950 50  0001 C CNN "PN"
	1    2750 3950
	-1   0    0    1   
$EndComp
$Comp
L obc_board-rescue:M95256-WMN6P-Memory_EEPROM PermMem?
U 1 1 604BA25C
P 2750 3250
AR Path="/604BA25C" Ref="PermMem?"  Part="1" 
AR Path="/6026EB4B/604BA25C" Ref="PermMem0"  Part="1" 
F 0 "PermMem0" H 3400 3050 50  0000 C CNN
F 1 "CAT25256" H 3200 3000 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 2750 3250 50  0001 C CNN
F 3 "https://www.onsemi.com/pdf/datasheet/cat25256-d.pdf" H 2750 3250 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/on-semiconductor/CAT25256VI-GT3/CAT25256VI-GT3OSCT-ND/2704984" H 2750 3250 50  0001 C CNN "Digikey"
F 5 "0,53" H 2750 3250 50  0001 C CNN "Cost"
F 6 "Ignore" H 2750 3250 50  0001 C CNN "PN"
	1    2750 3250
	-1   0    0    1   
$EndComp
$Comp
L obc_board-rescue:M95256-WMN6P-Memory_EEPROM PermMem?
U 1 1 604BA265
P 2750 4650
AR Path="/604BA265" Ref="PermMem?"  Part="1" 
AR Path="/6026EB4B/604BA265" Ref="PermMem2"  Part="1" 
F 0 "PermMem2" H 3450 4450 50  0000 C CNN
F 1 "CAT25256" H 3200 4400 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 2750 4650 50  0001 C CNN
F 3 "https://www.onsemi.com/pdf/datasheet/cat25256-d.pdf" H 2750 4650 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/on-semiconductor/CAT25256VI-GT3/CAT25256VI-GT3OSCT-ND/2704984" H 2750 4650 50  0001 C CNN "Digikey"
F 5 "2,20" H 2750 4650 50  0001 C CNN "Cost"
F 6 "Ignore" H 2750 4650 50  0001 C CNN "PN"
	1    2750 4650
	-1   0    0    1   
$EndComp
Text GLabel 2750 4350 2    50   Input ~ 0
GND
Text GLabel 2350 4550 0    50   Input ~ 0
PermMemSDO
Text GLabel 2350 4650 0    50   Input ~ 0
PermMemSDI
Text GLabel 2350 4750 0    50   Input ~ 0
PermMemSClock
Text GLabel 7950 3150 2    50   Input ~ 0
PermMem6Select
Text GLabel 7550 3550 2    50   Input ~ 0
VDD_FLASH
$Comp
L obc_board-rescue:M95256-WMN6P-Memory_EEPROM PermMem?
U 1 1 604BA276
P 7550 3250
AR Path="/604BA276" Ref="PermMem?"  Part="1" 
AR Path="/6026EB4B/604BA276" Ref="PermMem6"  Part="1" 
F 0 "PermMem6" H 8200 3050 50  0000 C CNN
F 1 "CAT25256" H 8000 3000 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 7550 3250 50  0001 C CNN
F 3 "https://www.onsemi.com/pdf/datasheet/cat25256-d.pdf" H 7550 3250 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/on-semiconductor/CAT25256VI-GT3/CAT25256VI-GT3OSCT-ND/2704984" H 7550 3250 50  0001 C CNN "Digikey"
F 5 "2,20" H 7550 3250 50  0001 C CNN "Cost"
F 6 "Ignore" H 7550 3250 50  0001 C CNN "PN"
	1    7550 3250
	-1   0    0    1   
$EndComp
Text GLabel 7550 2950 2    50   Input ~ 0
GND
Text GLabel 7150 3150 0    50   Input ~ 0
PermMemSDO
Text GLabel 7150 3250 0    50   Input ~ 0
PermMemSDI
Text GLabel 7150 3350 0    50   Input ~ 0
PermMemSClock
Text GLabel 5550 3850 2    50   Input ~ 0
PermMem4Select
Text GLabel 5150 4250 2    50   Input ~ 0
VDD_FLASH
$Comp
L obc_board-rescue:M95256-WMN6P-Memory_EEPROM PermMem?
U 1 1 604BA287
P 5150 3950
AR Path="/604BA287" Ref="PermMem?"  Part="1" 
AR Path="/6026EB4B/604BA287" Ref="PermMem4"  Part="1" 
F 0 "PermMem4" H 5800 3750 50  0000 C CNN
F 1 "CAT25256" H 5600 3700 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 5150 3950 50  0001 C CNN
F 3 "https://www.onsemi.com/pdf/datasheet/cat25256-d.pdf" H 5150 3950 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/on-semiconductor/CAT25256VI-GT3/CAT25256VI-GT3OSCT-ND/2704984" H 5150 3950 50  0001 C CNN "Digikey"
F 5 "2,20" H 5150 3950 50  0001 C CNN "Cost"
F 6 "Ignore" H 5150 3950 50  0001 C CNN "PN"
	1    5150 3950
	-1   0    0    1   
$EndComp
Text GLabel 5150 3650 2    50   Input ~ 0
GND
Text GLabel 4750 3850 0    50   Input ~ 0
PermMemSDO
Text GLabel 4750 3950 0    50   Input ~ 0
PermMemSDI
Text GLabel 4750 4050 0    50   Input ~ 0
PermMemSClock
Text GLabel 7950 3850 2    50   Input ~ 0
PermMem7Select
Text GLabel 7550 4250 2    50   Input ~ 0
VDD_FLASH
$Comp
L obc_board-rescue:M95256-WMN6P-Memory_EEPROM PermMem?
U 1 1 604BA298
P 7550 3950
AR Path="/604BA298" Ref="PermMem?"  Part="1" 
AR Path="/6026EB4B/604BA298" Ref="PermMem7"  Part="1" 
F 0 "PermMem7" H 8200 3750 50  0000 C CNN
F 1 "CAT25256" H 8000 3700 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 7550 3950 50  0001 C CNN
F 3 "https://www.onsemi.com/pdf/datasheet/cat25256-d.pdf" H 7550 3950 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/on-semiconductor/CAT25256VI-GT3/CAT25256VI-GT3OSCT-ND/2704984" H 7550 3950 50  0001 C CNN "Digikey"
F 5 "2,20" H 7550 3950 50  0001 C CNN "Cost"
F 6 "Ignore" H 7550 3950 50  0001 C CNN "PN"
	1    7550 3950
	-1   0    0    1   
$EndComp
Text GLabel 7550 3650 2    50   Input ~ 0
GND
Text GLabel 7150 3850 0    50   Input ~ 0
PermMemSDO
Text GLabel 7150 3950 0    50   Input ~ 0
PermMemSDI
Text GLabel 7150 4050 0    50   Input ~ 0
PermMemSClock
Text GLabel 5550 4550 2    50   Input ~ 0
PermMem5Select
Text GLabel 5150 4950 2    50   Input ~ 0
VDD_FLASH
$Comp
L obc_board-rescue:M95256-WMN6P-Memory_EEPROM PermMem?
U 1 1 604BA2A9
P 5150 4650
AR Path="/604BA2A9" Ref="PermMem?"  Part="1" 
AR Path="/6026EB4B/604BA2A9" Ref="PermMem5"  Part="1" 
F 0 "PermMem5" H 5800 4450 50  0000 C CNN
F 1 "CAT25256" H 5600 4400 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 5150 4650 50  0001 C CNN
F 3 "https://www.onsemi.com/pdf/datasheet/cat25256-d.pdf" H 5150 4650 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/on-semiconductor/CAT25256VI-GT3/CAT25256VI-GT3OSCT-ND/2704984" H 5150 4650 50  0001 C CNN "Digikey"
F 5 "2,20" H 5150 4650 50  0001 C CNN "Cost"
F 6 "Ignore" H 5150 4650 50  0001 C CNN "PN"
	1    5150 4650
	-1   0    0    1   
$EndComp
Text GLabel 5150 4350 2    50   Input ~ 0
GND
Text GLabel 4750 4550 0    50   Input ~ 0
PermMemSDO
Text GLabel 4750 4650 0    50   Input ~ 0
PermMemSDI
Text GLabel 4750 4750 0    50   Input ~ 0
PermMemSClock
Text GLabel 7950 4550 2    50   Input ~ 0
PermMem8Select
Text GLabel 7550 4950 2    50   Input ~ 0
VDD_FLASH
$Comp
L obc_board-rescue:M95256-WMN6P-Memory_EEPROM PermMem?
U 1 1 604BA2BA
P 7550 4650
AR Path="/604BA2BA" Ref="PermMem?"  Part="1" 
AR Path="/6026EB4B/604BA2BA" Ref="PermMem8"  Part="1" 
F 0 "PermMem8" H 8200 4450 50  0000 C CNN
F 1 "CAT25256" H 8000 4400 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 7550 4650 50  0001 C CNN
F 3 "https://www.onsemi.com/pdf/datasheet/cat25256-d.pdf" H 7550 4650 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/on-semiconductor/CAT25256VI-GT3/CAT25256VI-GT3OSCT-ND/2704984" H 7550 4650 50  0001 C CNN "Digikey"
F 5 "2,20" H 7550 4650 50  0001 C CNN "Cost"
F 6 "Ignore" H 7550 4650 50  0001 C CNN "PN"
	1    7550 4650
	-1   0    0    1   
$EndComp
Text GLabel 7550 4350 2    50   Input ~ 0
GND
Text GLabel 7150 4550 0    50   Input ~ 0
PermMemSDO
Text GLabel 7150 4650 0    50   Input ~ 0
PermMemSDI
Text GLabel 7150 4750 0    50   Input ~ 0
PermMemSClock
Text GLabel 6350 2500 2    50   Input ~ 0
PermMem4Select
Text GLabel 6350 2600 2    50   Input ~ 0
PermMem5Select
Text GLabel 6350 2700 2    50   Input ~ 0
PermMem6Select
Text GLabel 6350 2800 2    50   Input ~ 0
PermMem7Select
Text GLabel 6250 2600 0    50   Input ~ 0
SPI_1_CS_5
Text GLabel 6250 2700 0    50   Input ~ 0
SPI_1_CS_6
Text GLabel 6250 2800 0    50   Input ~ 0
SPI_1_CS_7
Text GLabel 6250 2500 0    50   Input ~ 0
SPI_1_CS_4
Wire Wire Line
	6350 2500 6250 2500
Wire Wire Line
	6250 2600 6350 2600
Wire Wire Line
	6350 2700 6250 2700
Wire Wire Line
	6250 2800 6350 2800
Text GLabel 6350 2900 2    50   Input ~ 0
PermMem8Select
Text GLabel 6250 2900 0    50   Input ~ 0
SPI_1_CS_8
Wire Wire Line
	6250 2900 6350 2900
Text Notes 2500 2550 0    50   ~ 0
FRAM / EEPROM
Wire Notes Line
	1650 5100 1650 2400
Wire Notes Line
	10050 2400 10050 5100
Wire Notes Line
	1650 5100 10050 5100
Wire Notes Line
	1650 2400 10050 2400
$Comp
L obc_board-rescue:TestPoint-Connector TP?
U 1 1 604BA30F
P 7950 3150
AR Path="/604BA30F" Ref="TP?"  Part="1" 
AR Path="/6026EB4B/604BA30F" Ref="TP6"  Part="1" 
F 0 "TP6" H 7850 3350 50  0000 L CNN
F 1 "TestPoint" H 8000 3300 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 8150 3150 50  0001 C CNN
F 3 "~" H 8150 3150 50  0001 C CNN
F 4 "Ignore" H 7950 3150 50  0001 C CNN "PN"
	1    7950 3150
	1    0    0    -1  
$EndComp
$Comp
L obc_board-rescue:TestPoint-Connector TP?
U 1 1 604BA316
P 7950 3850
AR Path="/604BA316" Ref="TP?"  Part="1" 
AR Path="/6026EB4B/604BA316" Ref="TP7"  Part="1" 
F 0 "TP7" H 7850 4050 50  0000 L CNN
F 1 "TestPoint" H 8000 4000 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 8150 3850 50  0001 C CNN
F 3 "~" H 8150 3850 50  0001 C CNN
F 4 "Ignore" H 7950 3850 50  0001 C CNN "PN"
	1    7950 3850
	1    0    0    -1  
$EndComp
$Comp
L obc_board-rescue:TestPoint-Connector TP?
U 1 1 604BA31D
P 7950 4550
AR Path="/604BA31D" Ref="TP?"  Part="1" 
AR Path="/6026EB4B/604BA31D" Ref="TP8"  Part="1" 
F 0 "TP8" H 7850 4750 50  0000 L CNN
F 1 "TestPoint" H 8000 4700 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 8150 4550 50  0001 C CNN
F 3 "~" H 8150 4550 50  0001 C CNN
F 4 "Ignore" H 7950 4550 50  0001 C CNN "PN"
	1    7950 4550
	1    0    0    -1  
$EndComp
$Comp
L obc_board-rescue:TestPoint-Connector TP?
U 1 1 604BA324
P 5550 3150
AR Path="/604BA324" Ref="TP?"  Part="1" 
AR Path="/6026EB4B/604BA324" Ref="TP3"  Part="1" 
F 0 "TP3" H 5450 3350 50  0000 L CNN
F 1 "TestPoint" H 5600 3300 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 5750 3150 50  0001 C CNN
F 3 "~" H 5750 3150 50  0001 C CNN
F 4 "Ignore" H 5550 3150 50  0001 C CNN "PN"
	1    5550 3150
	1    0    0    -1  
$EndComp
$Comp
L obc_board-rescue:TestPoint-Connector TP?
U 1 1 604BA32B
P 5550 3850
AR Path="/604BA32B" Ref="TP?"  Part="1" 
AR Path="/6026EB4B/604BA32B" Ref="TP4"  Part="1" 
F 0 "TP4" H 5450 4050 50  0000 L CNN
F 1 "TestPoint" H 5600 4000 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 5750 3850 50  0001 C CNN
F 3 "~" H 5750 3850 50  0001 C CNN
F 4 "Ignore" H 5550 3850 50  0001 C CNN "PN"
	1    5550 3850
	1    0    0    -1  
$EndComp
$Comp
L obc_board-rescue:TestPoint-Connector TP?
U 1 1 604BA332
P 5550 4550
AR Path="/604BA332" Ref="TP?"  Part="1" 
AR Path="/6026EB4B/604BA332" Ref="TP5"  Part="1" 
F 0 "TP5" H 5450 4750 50  0000 L CNN
F 1 "TestPoint" H 5600 4700 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 5750 4550 50  0001 C CNN
F 3 "~" H 5750 4550 50  0001 C CNN
F 4 "Ignore" H 5550 4550 50  0001 C CNN "PN"
	1    5550 4550
	1    0    0    -1  
$EndComp
$Comp
L obc_board-rescue:TestPoint-Connector TP?
U 1 1 604BA339
P 3150 3150
AR Path="/604BA339" Ref="TP?"  Part="1" 
AR Path="/6026EB4B/604BA339" Ref="TP0"  Part="1" 
F 0 "TP0" H 3050 3350 50  0000 L CNN
F 1 "TestPoint" H 3200 3300 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 3350 3150 50  0001 C CNN
F 3 "~" H 3350 3150 50  0001 C CNN
F 4 "Ignore" H 3150 3150 50  0001 C CNN "PN"
	1    3150 3150
	1    0    0    -1  
$EndComp
$Comp
L obc_board-rescue:TestPoint-Connector TP?
U 1 1 604BA340
P 3150 3850
AR Path="/604BA340" Ref="TP?"  Part="1" 
AR Path="/6026EB4B/604BA340" Ref="TP1"  Part="1" 
F 0 "TP1" H 3050 4050 50  0000 L CNN
F 1 "TestPoint" H 3200 4000 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 3350 3850 50  0001 C CNN
F 3 "~" H 3350 3850 50  0001 C CNN
F 4 "Ignore" H 3150 3850 50  0001 C CNN "PN"
	1    3150 3850
	1    0    0    -1  
$EndComp
$Comp
L obc_board-rescue:TestPoint-Connector TP?
U 1 1 604BA347
P 3150 4550
AR Path="/604BA347" Ref="TP?"  Part="1" 
AR Path="/6026EB4B/604BA347" Ref="TP2"  Part="1" 
F 0 "TP2" H 3050 4750 50  0000 L CNN
F 1 "TestPoint" H 3200 4700 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 3350 4550 50  0001 C CNN
F 3 "~" H 3350 4550 50  0001 C CNN
F 4 "Ignore" H 3150 4550 50  0001 C CNN "PN"
	1    3150 4550
	1    0    0    -1  
$EndComp
$Comp
L obc_board-rescue:SolderJumper_2_Open-Jumper JP?
U 1 1 604BA34E
P 8150 2650
AR Path="/604BA34E" Ref="JP?"  Part="1" 
AR Path="/6026EB4B/604BA34E" Ref="JP10"  Part="1" 
F 0 "JP10" H 8150 2855 50  0000 C CNN
F 1 "SolderJumper_2_Open" H 8150 2764 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_TrianglePad1.0x1.5mm" H 8150 2650 50  0001 C CNN
F 3 "~" H 8150 2650 50  0001 C CNN
F 4 "Ignore" H 8150 2650 50  0001 C CNN "PN"
	1    8150 2650
	1    0    0    -1  
$EndComp
Text GLabel 7900 2650 0    50   Input ~ 0
VDD
Text GLabel 8400 2650 2    50   Input ~ 0
VDD_FLASH
Wire Wire Line
	7900 2650 8000 2650
Wire Wire Line
	8300 2650 8400 2650
Text GLabel 4400 6150 2    50   Input ~ 0
GPIO_00
Text GLabel 4400 6850 2    50   Input ~ 0
SPI_1_CS_9
Text GLabel 4400 6050 2    50   Input ~ 0
GPIO_01
$Comp
L obc_board-rescue:Micro_SD_Card_Det-Connector J?
U 1 1 605927D3
P 3350 6550
AR Path="/605927D3" Ref="J?"  Part="1" 
AR Path="/6026348E/605927D3" Ref="J?"  Part="1" 
AR Path="/6026EB4B/605927D3" Ref="J8"  Part="1" 
F 0 "J8" H 3254 7230 50  0000 L CNN
F 1 "MEM2061-01-188-00-A " H 3345 7230 50  0000 L CNN
F 2 "würth_micro_sd:693071010811" H 5400 7250 50  0001 C CNN
F 3 "https://gct.co/files/drawings/mem2061.pdf" H 3350 6650 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/gct/MEM2061-01-188-00-A/2073-MEM2061-01-188-00-ACT-ND/9859683" V 3350 6550 50  0001 C CNN "Digikey"
F 5 "1,05" V 3350 6550 50  0001 C CNN "Cost"
F 6 "X" H 3350 6550 50  0001 C CNN "PN"
	1    3350 6550
	-1   0    0    1   
$EndComp
Text GLabel 2550 6050 0    50   Input ~ 0
GND
Text GLabel 5100 7050 3    50   Input ~ 0
VDD
Text GLabel 4400 6550 2    50   Input ~ 0
SPI_1_SCK
Text GLabel 4400 6350 2    50   Input ~ 0
SPI_1_MISO
Text GLabel 4400 6750 2    50   Input ~ 0
SPI_1_MOSI
Wire Wire Line
	4950 6450 4250 6450
Wire Wire Line
	4400 6550 4250 6550
Wire Wire Line
	4400 6750 4250 6750
Wire Wire Line
	4400 6850 4250 6850
Wire Wire Line
	4400 6350 4250 6350
Wire Wire Line
	4400 6150 4250 6150
Wire Wire Line
	4400 6050 4250 6050
NoConn ~ 4250 6250
NoConn ~ 4250 6950
Wire Notes Line
	2250 5750 5400 5750
Wire Notes Line
	5400 5750 5400 7350
Wire Notes Line
	5400 7350 2250 7350
Wire Notes Line
	2250 7350 2250 5750
Text Notes 2300 5850 0    50   ~ 0
Micro SD Card slot
$Comp
L obc_board-rescue:SolderJumper_2_Open-Jumper JP?
U 1 1 605927FC
P 5100 6800
AR Path="/605927FC" Ref="JP?"  Part="1" 
AR Path="/6026348E/605927FC" Ref="JP?"  Part="1" 
AR Path="/6026EB4B/605927FC" Ref="JP12"  Part="1" 
F 0 "JP12" V 5100 7005 50  0000 C CNN
F 1 "SolderJumper_2_Open" H 5200 6900 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_TrianglePad1.0x1.5mm" H 5100 6800 50  0001 C CNN
F 3 "~" H 5100 6800 50  0001 C CNN
F 4 "Ignore" H 5100 6800 50  0001 C CNN "PN"
	1    5100 6800
	0    1    1    0   
$EndComp
Wire Wire Line
	5100 7050 5100 6950
Connection ~ 4950 6450
Wire Wire Line
	4950 6650 4250 6650
Connection ~ 4950 6650
Wire Wire Line
	4950 6650 5100 6650
Wire Wire Line
	5100 6450 4950 6450
Text GLabel 5100 6450 2    50   Input ~ 0
GND
$Comp
L obc_board-rescue:C_Small-Device C?
U 1 1 605927DE
P 4950 6550
AR Path="/605927DE" Ref="C?"  Part="1" 
AR Path="/6026348E/605927DE" Ref="C?"  Part="1" 
AR Path="/6026EB4B/605927DE" Ref="C26"  Part="1" 
F 0 "C26" V 5000 6650 50  0000 L CNN
F 1 "100n" V 5050 6450 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 4950 6550 50  0001 C CNN
F 3 "https://www.yageo.com/upload/media/product/productsearch/datasheet/mlcc/UPY-GPHC_X7R_6.3V-to-50V_18.pdf" H 4950 6550 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/yageo/CC0603KPX7R7BB104/311-1335-1-ND/2103119" V 4950 6550 50  0001 C CNN "Digikey"
F 5 "0,08" V 4950 6550 50  0001 C CNN "Cost"
F 6 "C" H 4950 6550 50  0001 C CNN "PN"
	1    4950 6550
	1    0    0    -1  
$EndComp
Text GLabel 3150 3250 2    50   Input ~ 0
VDD_FLASH
Text GLabel 3150 3350 2    50   Input ~ 0
VDD_FLASH
Text GLabel 3150 3950 2    50   Input ~ 0
VDD_FLASH
Text GLabel 3150 4050 2    50   Input ~ 0
VDD_FLASH
Text GLabel 3150 4650 2    50   Input ~ 0
VDD_FLASH
Text GLabel 3150 4750 2    50   Input ~ 0
VDD_FLASH
Text GLabel 5550 3250 2    50   Input ~ 0
VDD_FLASH
Text GLabel 5550 3350 2    50   Input ~ 0
VDD_FLASH
Text GLabel 5550 3950 2    50   Input ~ 0
VDD_FLASH
Text GLabel 5550 4050 2    50   Input ~ 0
VDD_FLASH
Text GLabel 5550 4650 2    50   Input ~ 0
VDD_FLASH
Text GLabel 5550 4750 2    50   Input ~ 0
VDD_FLASH
Text GLabel 7950 3250 2    50   Input ~ 0
VDD_FLASH
Text GLabel 7950 3350 2    50   Input ~ 0
VDD_FLASH
Text GLabel 7950 3950 2    50   Input ~ 0
VDD_FLASH
Text GLabel 7950 4050 2    50   Input ~ 0
VDD_FLASH
Text GLabel 7950 4650 2    50   Input ~ 0
VDD_FLASH
Text GLabel 7950 4750 2    50   Input ~ 0
VDD_FLASH
$EndSCHEMATC
