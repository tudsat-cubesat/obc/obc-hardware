EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 6 10
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text GLabel 2950 2950 0    50   Input ~ 0
AS_0
Text GLabel 2950 3050 0    50   Input ~ 0
AS_1
Text GLabel 2950 3150 0    50   Input ~ 0
AS_2
Text GLabel 2950 3250 0    50   Input ~ 0
AS_3
Text GLabel 2950 3650 0    50   Input ~ 0
AS_Enable
Text GLabel 2950 2550 0    50   Input ~ 0
A_Com
Text GLabel 3950 2350 2    50   Input ~ 0
A_In_0
Text GLabel 3950 2450 2    50   Input ~ 0
A_In_1
Text GLabel 3950 2550 2    50   Input ~ 0
A_In_2
Text GLabel 3950 2650 2    50   Input ~ 0
A_In_3
Text GLabel 3950 2750 2    50   Input ~ 0
A_In_4
Text GLabel 3950 2850 2    50   Input ~ 0
A_In_5
Text GLabel 3950 2950 2    50   Input ~ 0
A_In_6
Text GLabel 3950 3050 2    50   Input ~ 0
A_In_7
Text GLabel 3950 3150 2    50   Input ~ 0
A_In_8
Text GLabel 3950 3250 2    50   Input ~ 0
A_In_9
Text GLabel 3950 3350 2    50   Input ~ 0
A_In_10
Text GLabel 3950 3450 2    50   Input ~ 0
A_In_11
Text GLabel 3950 3550 2    50   Input ~ 0
A_In_12
Text GLabel 3950 3650 2    50   Input ~ 0
A_In_13
Text GLabel 3950 3750 2    50   Input ~ 0
A_In_14
Text GLabel 3950 3850 2    50   Input ~ 0
A_In_15
Text GLabel 4700 3850 0    50   Input ~ 0
A_In_15
Text GLabel 4700 2350 0    50   Input ~ 0
A_In_0
Text GLabel 4700 2450 0    50   Input ~ 0
A_In_1
$Comp
L obc_board-rescue:Conn_01x16_Female-Connector J?
U 1 1 60589C3E
P 4900 3050
AR Path="/60589C3E" Ref="J?"  Part="1" 
AR Path="/6026CC97/60589C3E" Ref="J1"  Part="1" 
F 0 "J1" H 4550 3850 50  0000 L CNN
F 1 "1x16_Female_AIn" H 4250 3950 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x16_P2.54mm_Vertical_SMD_Pin1Left" H 4900 3050 50  0001 C CNN
F 3 "~" H 4900 3050 50  0001 C CNN
F 4 "Ignore" H 4900 3050 50  0001 C CNN "PN"
	1    4900 3050
	1    0    0    -1  
$EndComp
Text GLabel 4700 3750 0    50   Input ~ 0
A_In_14
Text GLabel 4700 3650 0    50   Input ~ 0
A_In_13
Text GLabel 4700 3550 0    50   Input ~ 0
A_In_12
Text GLabel 4700 3450 0    50   Input ~ 0
A_In_11
Text GLabel 4700 3350 0    50   Input ~ 0
A_In_10
Text GLabel 4700 3250 0    50   Input ~ 0
A_In_9
Text GLabel 4700 3150 0    50   Input ~ 0
A_In_8
Text GLabel 4700 3050 0    50   Input ~ 0
A_In_7
Text GLabel 4700 2950 0    50   Input ~ 0
A_In_6
Text GLabel 4700 2850 0    50   Input ~ 0
A_In_5
Text GLabel 4700 2750 0    50   Input ~ 0
A_In_4
Text GLabel 4700 2650 0    50   Input ~ 0
A_In_3
Text GLabel 4700 2550 0    50   Input ~ 0
A_In_2
$Comp
L obc_board-rescue:CD74HC4067M-74xx U?
U 1 1 60589C54
P 3450 3050
AR Path="/60589C54" Ref="U?"  Part="1" 
AR Path="/6026CC97/60589C54" Ref="U1"  Part="1" 
F 0 "U1" H 3300 4050 50  0000 C CNN
F 1 "CD74HC4067M" H 3100 3950 50  0000 C CNN
F 2 "Package_SO:SOIC-24W_7.5x15.4mm_P1.27mm" H 4350 2050 50  0001 C CIN
F 3 "http://www.ti.com/lit/ds/symlink/cd74hc4067.pdf" H 3100 3900 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/texas-instruments/CD74HC4067M96/296-29408-1-ND/2741760" H 3450 3050 50  0001 C CNN "Digikey"
F 5 "0,64" H 3450 3050 50  0001 C CNN "Cost"
F 6 "U" H 3450 3050 50  0001 C CNN "PN"
	1    3450 3050
	1    0    0    -1  
$EndComp
Text Notes 2600 1850 0    50   ~ 0
Voltage Measurement for PCDU
Text GLabel 3650 2050 2    50   Input ~ 0
VDD
Text GLabel 3450 4150 2    50   Input ~ 0
GND
Wire Notes Line
	4950 1750 4950 4450
Wire Notes Line
	2450 1750 2450 4450
$Comp
L obc_board-rescue:Ferrite_Bead_Small-Device FB?
U 1 1 60589C64
P 3550 2050
AR Path="/60589C64" Ref="FB?"  Part="1" 
AR Path="/6026CC97/60589C64" Ref="FB3"  Part="1" 
F 0 "FB3" V 3650 1950 50  0000 C CNN
F 1 "1k/600" V 3700 2200 50  0001 C CNN
F 2 "Inductor_SMD:L_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 3480 2050 50  0001 C CNN
F 3 "https://www.we-online.de/katalog/datasheet/742792662.pdf" H 3550 2050 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/w%C3%BCrth-elektronik/742792662/732-4670-1-ND/4310442" V 3550 2050 50  0001 C CNN "Digikey"
F 5 "0,14" V 3550 2050 50  0001 C CNN "Cost"
F 6 "Ignore" H 3550 2050 50  0001 C CNN "PN"
	1    3550 2050
	0    -1   -1   0   
$EndComp
$Comp
L obc_board-rescue:ICM-20948-Sensor_Motion U?
U 1 1 60589C6D
P 6500 2850
AR Path="/60589C6D" Ref="U?"  Part="1" 
AR Path="/6026CC97/60589C6D" Ref="U3"  Part="1" 
F 0 "U3" H 5850 3400 50  0000 C CNN
F 1 "ICM-20648" H 5850 3500 50  0000 C CNN
F 2 "Package_DFN_QFN:QFN-24-1EP_3x3mm_P0.4mm_EP1.75x1.6mm" H 6500 1850 50  0001 C CNN
F 3 "http://www.invensense.com/wp-content/uploads/2016/06/DS-000189-ICM-20948-v1.3.pdf" H 6500 2700 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/tdk-invensense/ICM-20648/1428-1061-1-ND/5872876" H 6500 2850 50  0001 C CNN "Digikey"
F 5 "6,54" H 6500 2850 50  0001 C CNN "Cost"
F 6 "U" H 6500 2850 50  0001 C CNN "PN"
	1    6500 2850
	1    0    0    -1  
$EndComp
Text GLabel 6000 2750 0    50   Input ~ 0
I2C_2_SCL
Text GLabel 6000 2650 0    50   Input ~ 0
I2C_2_SDA
Text GLabel 6000 2850 0    50   Input ~ 0
VDD_ACC
NoConn ~ 7000 2750
NoConn ~ 7000 2850
Text GLabel 6600 1850 2    50   Input ~ 0
VDD_ACC
Text GLabel 7100 3050 1    50   Input ~ 0
VDD_ACC
Wire Wire Line
	6400 2150 6400 2050
Wire Wire Line
	6600 2150 6600 2050
Wire Wire Line
	7000 3050 7100 3050
NoConn ~ 6000 3050
NoConn ~ 6000 3150
Text Notes 5600 1900 0    50   ~ 0
Accelerometer
Text GLabel 6200 2050 0    50   Input ~ 0
GND
Text GLabel 6800 2050 2    50   Input ~ 0
GND
Text GLabel 7100 3250 3    50   Input ~ 0
GND
Text GLabel 6500 3550 2    50   Input ~ 0
GND
Wire Wire Line
	6400 1850 6600 1850
NoConn ~ 6000 2550
Wire Notes Line
	7200 1750 7200 3650
Wire Notes Line
	5300 3650 5300 1750
$Comp
L obc_board-rescue:Ferrite_Bead_Small-Device FB?
U 1 1 60589C8C
P 6400 1950
AR Path="/60589C8C" Ref="FB?"  Part="1" 
AR Path="/6026CC97/60589C8C" Ref="FB14"  Part="1" 
F 0 "FB14" H 6500 1850 50  0000 C CNN
F 1 "1k/600" V 6550 2100 50  0001 C CNN
F 2 "Inductor_SMD:L_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 6330 1950 50  0001 C CNN
F 3 "https://www.we-online.de/katalog/datasheet/742792662.pdf" H 6400 1950 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/w%C3%BCrth-elektronik/742792662/732-4670-1-ND/4310442" V 6400 1950 50  0001 C CNN "Digikey"
F 5 "0,14" V 6400 1950 50  0001 C CNN "Cost"
F 6 "Ignore" H 6400 1950 50  0001 C CNN "PN"
	1    6400 1950
	-1   0    0    1   
$EndComp
$Comp
L obc_board-rescue:Ferrite_Bead_Small-Device FB?
U 1 1 60589C95
P 6600 1950
AR Path="/60589C95" Ref="FB?"  Part="1" 
AR Path="/6026CC97/60589C95" Ref="FB15"  Part="1" 
F 0 "FB15" H 6700 1800 50  0000 C CNN
F 1 "1k/600" V 6750 2100 50  0001 C CNN
F 2 "Inductor_SMD:L_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 6530 1950 50  0001 C CNN
F 3 "https://www.we-online.de/katalog/datasheet/742792662.pdf" H 6600 1950 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/w%C3%BCrth-elektronik/742792662/732-4670-1-ND/4310442" V 6600 1950 50  0001 C CNN "Digikey"
F 5 "0,14" V 6600 1950 50  0001 C CNN "Cost"
F 6 "Ignore" H 6600 1950 50  0001 C CNN "PN"
	1    6600 1950
	-1   0    0    1   
$EndComp
$Comp
L obc_board-rescue:C_Small-Device C?
U 1 1 60589C9E
P 6300 2050
AR Path="/60589C9E" Ref="C?"  Part="1" 
AR Path="/6026CC97/60589C9E" Ref="C17"  Part="1" 
F 0 "C17" V 6400 2100 50  0000 L CNN
F 1 "100n" V 6200 2050 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 6300 2050 50  0001 C CNN
F 3 "https://www.yageo.com/upload/media/product/productsearch/datasheet/mlcc/UPY-GPHC_X7R_6.3V-to-50V_18.pdf" H 6300 2050 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/yageo/CC0603KPX7R7BB104/311-1335-1-ND/2103119" V 6300 2050 50  0001 C CNN "Digikey"
F 5 "0,08" V 6300 2050 50  0001 C CNN "Cost"
F 6 "C" H 6300 2050 50  0001 C CNN "PN"
	1    6300 2050
	0    -1   -1   0   
$EndComp
$Comp
L obc_board-rescue:C_Small-Device C?
U 1 1 60589CA7
P 6700 2050
AR Path="/60589CA7" Ref="C?"  Part="1" 
AR Path="/6026CC97/60589CA7" Ref="C18"  Part="1" 
F 0 "C18" V 6800 1900 50  0000 L CNN
F 1 "100n" V 6600 1850 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 6700 2050 50  0001 C CNN
F 3 "https://www.yageo.com/upload/media/product/productsearch/datasheet/mlcc/UPY-GPHC_X7R_6.3V-to-50V_18.pdf" H 6700 2050 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/yageo/CC0603KPX7R7BB104/311-1335-1-ND/2103119" V 6700 2050 50  0001 C CNN "Digikey"
F 5 "0,08" V 6700 2050 50  0001 C CNN "Cost"
F 6 "C" H 6700 2050 50  0001 C CNN "PN"
	1    6700 2050
	0    -1   -1   0   
$EndComp
$Comp
L obc_board-rescue:C_Small-Device C?
U 1 1 60589CB0
P 7100 3150
AR Path="/60589CB0" Ref="C?"  Part="1" 
AR Path="/6026CC97/60589CB0" Ref="C19"  Part="1" 
F 0 "C19" H 7150 3150 50  0000 L CNN
F 1 "100n" V 7200 3200 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 7100 3150 50  0001 C CNN
F 3 "https://www.yageo.com/upload/media/product/productsearch/datasheet/mlcc/UPY-GPHC_X7R_6.3V-to-50V_18.pdf" H 7100 3150 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/yageo/CC0603KPX7R7BB104/311-1335-1-ND/2103119" V 7100 3150 50  0001 C CNN "Digikey"
F 5 "0,08" V 7100 3150 50  0001 C CNN "Cost"
F 6 "C" H 7100 3150 50  0001 C CNN "PN"
	1    7100 3150
	-1   0    0    1   
$EndComp
$Comp
L obc_board-rescue:ZXBM5210-S-Driver_Motor U?
U 1 1 60589CB9
P 3700 5750
AR Path="/60589CB9" Ref="U?"  Part="1" 
AR Path="/6026CC97/60589CB9" Ref="U6"  Part="1" 
F 0 "U6" H 3450 5400 50  0000 C CNN
F 1 "ZXBM5210-S" H 3700 5250 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 3750 5500 50  0001 C CNN
F 3 "https://www.diodes.com/assets/Datasheets/ZXBM5210.pdf" H 3700 5750 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/diodes-incorporated/ZXBM5210-S-13/ZXBM5210-S-13DICT-ND/4567795" H 3700 5750 50  0001 C CNN "Digikey"
F 5 "0,83" H 3700 5750 50  0001 C CNN "Cost"
F 6 "U" H 3700 5750 50  0001 C CNN "PN"
	1    3700 5750
	1    0    0    -1  
$EndComp
Connection ~ 6400 2050
Connection ~ 6600 2050
Text GLabel 4200 5650 2    50   Input ~ 0
Motor1
Text GLabel 4200 5850 2    50   Input ~ 0
Motor2
$Comp
L obc_board-rescue:GND-power #PWR?
U 1 1 60589CC3
P 3700 6050
AR Path="/60589CC3" Ref="#PWR?"  Part="1" 
AR Path="/6026CC97/60589CC3" Ref="#PWR0101"  Part="1" 
F 0 "#PWR0101" H 3700 5800 50  0001 C CNN
F 1 "GND" H 3850 6000 50  0000 C CNN
F 2 "" H 3700 6050 50  0001 C CNN
F 3 "" H 3700 6050 50  0001 C CNN
	1    3700 6050
	1    0    0    -1  
$EndComp
Text GLabel 4100 4850 2    50   Input ~ 0
VDD
Text GLabel 3200 5650 0    50   Input ~ 0
VDD
$Comp
L obc_board-rescue:Conn_01x02_Female-Connector J?
U 1 1 60589CCC
P 4750 5200
AR Path="/60589CCC" Ref="J?"  Part="1" 
AR Path="/6026CC97/60589CCC" Ref="J9"  Part="1" 
F 0 "J9" V 4688 5012 50  0000 R CNN
F 1 "Conn_01x02_Female" V 4597 5012 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 4750 5200 50  0001 C CNN
F 3 "~" H 4750 5200 50  0001 C CNN
F 4 "Ignore" H 4750 5200 50  0001 C CNN "PN"
	1    4750 5200
	1    0    0    -1  
$EndComp
Text GLabel 4550 5300 0    50   Input ~ 0
Motor1
Text GLabel 4550 5200 0    50   Input ~ 0
Motor2
Text GLabel 3200 5750 0    50   Input ~ 0
TIM_2_CH_4
Text GLabel 3200 5850 0    50   Input ~ 0
TIM_2_CH_1
Wire Wire Line
	3200 5750 3300 5750
Wire Wire Line
	3200 5850 3300 5850
Text GLabel 3400 4950 1    50   Input ~ 0
GND
$Comp
L obc_board-rescue:C_Small-Device C?
U 1 1 60589CDC
P 3500 5250
AR Path="/60589CDC" Ref="C?"  Part="1" 
AR Path="/6026CC97/60589CDC" Ref="C27"  Part="1" 
F 0 "C27" V 3500 5450 50  0000 L CNN
F 1 "100n" V 3400 5150 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 3500 5250 50  0001 C CNN
F 3 "https://www.yageo.com/upload/media/product/productsearch/datasheet/mlcc/UPY-GPHC_X7R_6.3V-to-50V_18.pdf" H 3500 5250 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/yageo/CC0603KPX7R7BB104/311-1335-1-ND/2103119" V 3500 5250 50  0001 C CNN "Digikey"
F 5 "0,08" V 3500 5250 50  0001 C CNN "Cost"
F 6 "C" H 3500 5250 50  0001 C CNN "PN"
	1    3500 5250
	0    -1   -1   0   
$EndComp
$Comp
L obc_board-rescue:C_Small-Device C?
U 1 1 60589CE5
P 3500 5100
AR Path="/60589CE5" Ref="C?"  Part="1" 
AR Path="/6026CC97/60589CE5" Ref="C25"  Part="1" 
F 0 "C25" V 3500 4800 50  0000 L CNN
F 1 "1u" V 3400 5050 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 3500 5100 50  0001 C CNN
F 3 "http://weblib.samsungsem.com/mlcc/mlcc-ec-data-sheet.do?partNumber=CL10A105KP8NNN" H 3500 5100 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/samsung-electro-mechanics/CL10A105KP8NNNC/1276-1182-1-ND/3889268" V 3500 5100 50  0001 C CNN "Digikey"
F 5 "0,08" V 3500 5100 50  0001 C CNN "Cost"
F 6 "C" H 3500 5100 50  0001 C CNN "PN"
	1    3500 5100
	0    1    1    0   
$EndComp
Wire Wire Line
	3600 5350 3600 5250
Connection ~ 3600 5100
Connection ~ 3600 5250
Wire Wire Line
	3600 5250 3600 5100
Wire Wire Line
	3400 5250 3400 5100
Connection ~ 3400 5100
Wire Wire Line
	3400 5100 3400 4950
Wire Wire Line
	4100 5650 4200 5650
Wire Wire Line
	4100 5850 4200 5850
$Comp
L obc_board-rescue:C_Small-Device C?
U 1 1 60589CF7
P 3900 5250
AR Path="/60589CF7" Ref="C?"  Part="1" 
AR Path="/6026CC97/60589CF7" Ref="C28"  Part="1" 
F 0 "C28" V 3800 5200 50  0000 L CNN
F 1 "10u" V 3950 5300 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 3900 5250 50  0001 C CNN
F 3 "http://datasheets.avx.com/cx5r.pdf" H 3900 5250 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/avx-corporation/0603ZD106KAT2A/478-10766-1-ND/7536554" V 3900 5250 50  0001 C CNN "Digikey"
F 5 "0,1" V 3900 5250 50  0001 C CNN "Cost"
F 6 "C" H 3900 5250 50  0001 C CNN "PN"
	1    3900 5250
	0    1    1    0   
$EndComp
Wire Wire Line
	3800 5350 3800 5250
Wire Wire Line
	4000 5250 4000 5150
Text GLabel 4000 5150 1    50   Input ~ 0
GND
Wire Wire Line
	3200 5650 3300 5650
Wire Notes Line
	2650 4650 4800 4650
Wire Notes Line
	4800 4650 4800 6300
Wire Notes Line
	4800 6300 2650 6300
Wire Notes Line
	2650 6300 2650 4650
Text Notes 2750 4800 0    50   ~ 0
Motor Control
$Comp
L obc_board-rescue:SolderJumper_2_Open-Jumper JP?
U 1 1 60589D07
P 3850 4850
AR Path="/60589D07" Ref="JP?"  Part="1" 
AR Path="/6026CC97/60589D07" Ref="JP14"  Part="1" 
F 0 "JP14" H 3850 4750 50  0000 C CNN
F 1 "SolderJumper_2_Open" H 3900 4950 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_TrianglePad1.0x1.5mm" H 3850 4850 50  0001 C CNN
F 3 "~" H 3850 4850 50  0001 C CNN
F 4 "Ignore" H 3850 4850 50  0001 C CNN "PN"
	1    3850 4850
	1    0    0    -1  
$EndComp
$Comp
L obc_board-rescue:SolderJumper_2_Open-Jumper JP?
U 1 1 60589D0E
P 5450 3200
AR Path="/60589D0E" Ref="JP?"  Part="1" 
AR Path="/6026CC97/60589D0E" Ref="JP15"  Part="1" 
F 0 "JP15" H 5450 3405 50  0000 C CNN
F 1 "SolderJumper_2_Open" H 5450 3314 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_TrianglePad1.0x1.5mm" H 5450 3200 50  0001 C CNN
F 3 "~" H 5450 3200 50  0001 C CNN
F 4 "Ignore" H 5450 3200 50  0001 C CNN "PN"
	1    5450 3200
	0    1    1    0   
$EndComp
Wire Wire Line
	3600 4850 3700 4850
Wire Wire Line
	3600 4850 3600 5100
Wire Wire Line
	3800 5250 3600 5250
Connection ~ 3800 5250
Wire Wire Line
	4000 4850 4100 4850
Text GLabel 5450 3450 3    50   Input ~ 0
VDD
Text GLabel 5450 2950 1    50   Input ~ 0
VDD_ACC
Wire Wire Line
	5450 2950 5450 3050
Wire Wire Line
	5450 3350 5450 3450
Text GLabel 6450 5800 2    50   Input ~ 0
GPIO_02
$Comp
L obc_board-rescue:SW_Push-Switch SW?
U 1 1 6058FBEA
P 6250 5700
AR Path="/6058FBEA" Ref="SW?"  Part="1" 
AR Path="/6026444C/6058FBEA" Ref="SW?"  Part="1" 
AR Path="/6026CC97/6058FBEA" Ref="SW2"  Part="1" 
F 0 "SW2" H 6350 5800 50  0000 C CNN
F 1 "SW_Push" H 6050 5800 50  0000 C CNN
F 2 "Button_Switch_SMD:SW_SPST_PTS645" H 6250 5900 50  0001 C CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1977263&DocType=Customer+Drawing&DocLang=English" H 6250 5900 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/te-connectivity-alcoswitch-switches/FSM1LPSTR/450-2156-1-ND/5343836" H 6250 5700 50  0001 C CNN "Digikey"
F 5 "0,38" H 6250 5700 50  0001 C CNN "Cost"
F 6 "X" H 6250 5700 50  0001 C CNN "PN"
	1    6250 5700
	1    0    0    -1  
$EndComp
Text GLabel 5950 5700 0    50   Input ~ 0
VDD
Text GLabel 5950 5900 0    50   Input ~ 0
GND
$Comp
L obc_board-rescue:R-Device R?
U 1 1 6058FBF5
P 6250 5900
AR Path="/6058FBF5" Ref="R?"  Part="1" 
AR Path="/6026444C/6058FBF5" Ref="R?"  Part="1" 
AR Path="/6026CC97/6058FBF5" Ref="R9"  Part="1" 
F 0 "R9" V 6250 5900 50  0000 C CNN
F 1 "4.7k" V 6350 5900 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 6180 5900 50  0001 C CNN
F 3 "https://d1d2qsbl8m0m72.cloudfront.net/en/products/databook/datasheet/passive/resistor/chip_resistor/esr-e.pdf" H 6250 5900 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/rohm-semiconductor/ESR03EZPJ472/RHM4-7KDCT-ND/1762936" V 6250 5900 50  0001 C CNN "Digikey"
F 5 "0,08" V 6250 5900 50  0001 C CNN "Cost"
F 6 "R" H 6250 5900 50  0001 C CNN "PN"
	1    6250 5900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5950 5700 6050 5700
Wire Wire Line
	5950 5900 6100 5900
Wire Wire Line
	6450 5900 6400 5900
Wire Wire Line
	6450 5700 6450 5900
Text Notes 6000 5550 0    50   ~ 0
User Button
Wire Notes Line
	5700 5450 6850 5450
Wire Notes Line
	6850 5450 6850 6000
Wire Notes Line
	6850 6000 5700 6000
Wire Notes Line
	5700 6000 5700 5450
$Comp
L obc_board-rescue:LED-Device D?
U 1 1 6058FC07
P 6550 4400
AR Path="/6058FC07" Ref="D?"  Part="1" 
AR Path="/6026444C/6058FC07" Ref="D?"  Part="1" 
AR Path="/6026CC97/6058FC07" Ref="D4"  Part="1" 
F 0 "D4" H 6650 4450 50  0000 C CNN
F 1 "LED-BL" H 6200 4450 50  0000 C CNN
F 2 "Diode_SMD:D_0603_1608Metric" H 6550 4400 50  0001 C CNN
F 3 "~" H 6550 4400 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/rohm-semiconductor/SMLE13BC8TT86/846-1177-1-ND/5053794" H 6550 4400 50  0001 C CNN "Digikey"
F 5 "0,36" H 6550 4400 50  0001 C CNN "Cost"
F 6 "D" H 6550 4400 50  0001 C CNN "PN"
	1    6550 4400
	-1   0    0    1   
$EndComp
$Comp
L obc_board-rescue:LED-Device D?
U 1 1 6058FC10
P 6550 4600
AR Path="/6058FC10" Ref="D?"  Part="1" 
AR Path="/6026444C/6058FC10" Ref="D?"  Part="1" 
AR Path="/6026CC97/6058FC10" Ref="D5"  Part="1" 
F 0 "D5" H 6650 4650 50  0000 C CNN
F 1 "LED-GR" H 6200 4650 50  0000 C CNN
F 2 "Diode_SMD:D_0603_1608Metric" H 6550 4600 50  0001 C CNN
F 3 "~" H 6550 4600 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/rohm-semiconductor/SMLE12EC6TT86/846-SMLE12EC6TT86CT-ND/4111464" H 6550 4600 50  0001 C CNN "Digikey"
F 5 "1,09" H 6550 4600 50  0001 C CNN "Cost"
F 6 "D" H 6550 4600 50  0001 C CNN "PN"
	1    6550 4600
	-1   0    0    1   
$EndComp
$Comp
L obc_board-rescue:LED-Device D?
U 1 1 6058FC19
P 6550 4000
AR Path="/6058FC19" Ref="D?"  Part="1" 
AR Path="/6026444C/6058FC19" Ref="D?"  Part="1" 
AR Path="/6026CC97/6058FC19" Ref="D2"  Part="1" 
F 0 "D2" H 6650 4050 50  0000 C CNN
F 1 "LED-WH" H 6200 4050 50  0000 C CNN
F 2 "Diode_SMD:D_0603_1608Metric" H 6550 4000 50  0001 C CNN
F 3 "https://media.digikey.com/pdf/Data%20Sheets/Rohm%20PDFs/SML-31.pdf" H 6550 4000 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/rohm-semiconductor/SML312WBCW1/846-SML312WBCW1CT-ND/1971466" H 6550 4000 50  0001 C CNN "Digikey"
F 5 "0,59" H 6550 4000 50  0001 C CNN "Cost"
F 6 "D" H 6550 4000 50  0001 C CNN "PN"
	1    6550 4000
	-1   0    0    1   
$EndComp
Text GLabel 6100 4600 0    50   Input ~ 0
LED_Green
Text GLabel 6100 4400 0    50   Input ~ 0
LED_Blue
Text GLabel 6100 4200 0    50   Input ~ 0
LED_Red
Text GLabel 6100 4000 0    50   Input ~ 0
LED_White
Text Notes 5650 4000 0    50   ~ 0
LED Block\n\n
Connection ~ 6700 4400
Wire Wire Line
	6700 4400 6700 4600
Connection ~ 6700 4600
Wire Wire Line
	6700 4600 6700 4800
Text GLabel 6700 4800 2    50   Input ~ 0
GND
Wire Notes Line
	5600 4900 7100 4900
Wire Notes Line
	5600 3800 7100 3800
Wire Notes Line
	7100 4900 7100 3800
Wire Notes Line
	5600 3800 5600 4900
$Comp
L obc_board-rescue:R-Device R?
U 1 1 6058FC30
P 6250 4000
AR Path="/6058FC30" Ref="R?"  Part="1" 
AR Path="/6026444C/6058FC30" Ref="R?"  Part="1" 
AR Path="/6026CC97/6058FC30" Ref="R10"  Part="1" 
F 0 "R10" V 6250 3950 50  0000 L CNN
F 1 "680" V 6150 3900 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 6180 4000 50  0001 C CNN
F 3 "https://industrial.panasonic.com/cdbs/www-data/pdf/RDO0000/AOA0000C331.pdf" H 6250 4000 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/panasonic-electronic-components/ERJ-PA3J681V/P680BZCT-ND/5036355" V 6250 4000 50  0001 C CNN "Digikey"
F 5 "0,08" V 6250 4000 50  0001 C CNN "Cost"
F 6 "R" H 6250 4000 50  0001 C CNN "PN"
	1    6250 4000
	0    1    1    0   
$EndComp
$Comp
L obc_board-rescue:R-Device R?
U 1 1 6058FC39
P 6250 4200
AR Path="/6058FC39" Ref="R?"  Part="1" 
AR Path="/6026444C/6058FC39" Ref="R?"  Part="1" 
AR Path="/6026CC97/6058FC39" Ref="R11"  Part="1" 
F 0 "R11" V 6250 4150 50  0000 L CNN
F 1 "680" V 6150 4100 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 6180 4200 50  0001 C CNN
F 3 "https://industrial.panasonic.com/cdbs/www-data/pdf/RDO0000/AOA0000C331.pdf" H 6250 4200 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/panasonic-electronic-components/ERJ-PA3J681V/P680BZCT-ND/5036355" V 6250 4200 50  0001 C CNN "Digikey"
F 5 "0,08" V 6250 4200 50  0001 C CNN "Cost"
F 6 "R" H 6250 4200 50  0001 C CNN "PN"
	1    6250 4200
	0    1    1    0   
$EndComp
$Comp
L obc_board-rescue:R-Device R?
U 1 1 6058FC42
P 6250 4400
AR Path="/6058FC42" Ref="R?"  Part="1" 
AR Path="/6026444C/6058FC42" Ref="R?"  Part="1" 
AR Path="/6026CC97/6058FC42" Ref="R12"  Part="1" 
F 0 "R12" V 6250 4350 50  0000 L CNN
F 1 "680" V 6150 4300 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 6180 4400 50  0001 C CNN
F 3 "https://industrial.panasonic.com/cdbs/www-data/pdf/RDO0000/AOA0000C331.pdf" H 6250 4400 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/panasonic-electronic-components/ERJ-PA3J681V/P680BZCT-ND/5036355" V 6250 4400 50  0001 C CNN "Digikey"
F 5 "0,08" V 6250 4400 50  0001 C CNN "Cost"
F 6 "R" H 6250 4400 50  0001 C CNN "PN"
	1    6250 4400
	0    1    1    0   
$EndComp
$Comp
L obc_board-rescue:R-Device R?
U 1 1 6058FC4B
P 6250 4600
AR Path="/6058FC4B" Ref="R?"  Part="1" 
AR Path="/6026444C/6058FC4B" Ref="R?"  Part="1" 
AR Path="/6026CC97/6058FC4B" Ref="R13"  Part="1" 
F 0 "R13" V 6250 4550 50  0000 L CNN
F 1 "680" V 6150 4500 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 6180 4600 50  0001 C CNN
F 3 "https://industrial.panasonic.com/cdbs/www-data/pdf/RDO0000/AOA0000C331.pdf" H 6250 4600 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/panasonic-electronic-components/ERJ-PA3J681V/P680BZCT-ND/5036355" V 6250 4600 50  0001 C CNN "Digikey"
F 5 "0,08" V 6250 4600 50  0001 C CNN "Cost"
F 6 "R" H 6250 4600 50  0001 C CNN "PN"
	1    6250 4600
	0    1    1    0   
$EndComp
$Comp
L obc_board-rescue:LED-Device D?
U 1 1 6058FC54
P 6550 4200
AR Path="/6058FC54" Ref="D?"  Part="1" 
AR Path="/6026444C/6058FC54" Ref="D?"  Part="1" 
AR Path="/6026CC97/6058FC54" Ref="D3"  Part="1" 
F 0 "D3" H 6650 4250 50  0000 C CNN
F 1 "LED-RE" H 6200 4250 50  0000 C CNN
F 2 "LED_SMD:LED_0603_1608Metric" H 6550 4200 50  0001 C CNN
F 3 "https://media.digikey.com/pdf/Data%20Sheets/Rohm%20PDFs/SML-31_Series.pdf" H 6550 4200 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/rohm-semiconductor/SML-311UTT86/511-1304-1-ND/637117" H 6550 4200 50  0001 C CNN "Digikey"
F 5 "0,45" H 6550 4200 50  0001 C CNN "Cost"
F 6 "D" H 6550 4200 50  0001 C CNN "PN"
	1    6550 4200
	-1   0    0    1   
$EndComp
Wire Wire Line
	6700 4000 6700 4200
Connection ~ 6700 4200
Wire Wire Line
	6700 4200 6700 4400
$Comp
L obc_board-rescue:R-Device R?
U 1 1 6058FC60
P 3600 6950
AR Path="/6058FC60" Ref="R?"  Part="1" 
AR Path="/6026444C/6058FC60" Ref="R?"  Part="1" 
AR Path="/6026CC97/6058FC60" Ref="R3"  Part="1" 
F 0 "R3" V 3393 6950 50  0000 C CNN
F 1 "1k" V 3484 6950 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 3530 6950 50  0001 C CNN
F 3 "https://d1d2qsbl8m0m72.cloudfront.net/en/products/databook/datasheet/passive/resistor/chip_resistor/esr-e.pdf" H 3600 6950 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/rohm-semiconductor/ESR03EZPJ102/RHM1-0KDCT-ND/1762924" V 3600 6950 50  0001 C CNN "Digikey"
F 5 "0,08" V 3600 6950 50  0001 C CNN "Cost"
F 6 "R" H 3600 6950 50  0001 C CNN "PN"
	1    3600 6950
	0    1    1    0   
$EndComp
$Comp
L obc_board-rescue:Thermistor_NTC-Device TH?
U 1 1 6058FC69
P 3600 7150
AR Path="/6058FC69" Ref="TH?"  Part="1" 
AR Path="/6026444C/6058FC69" Ref="TH?"  Part="1" 
AR Path="/6026CC97/6058FC69" Ref="TH1"  Part="1" 
F 0 "TH1" V 3800 7100 50  0000 L CNN
F 1 "10k" V 3698 7105 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P2.54mm_Vertical" H 3600 7200 50  0001 C CNN
F 3 "https://www.littelfuse.com/~/media/electronics/datasheets/leaded_thermistors/littelfuse_leaded_thermistors_glass_encapsulated_thermistors_do_35_standard_datasheet.pdf.pdf" H 3600 7200 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/littelfuse-inc/103JG1F/615-1024-ND/1014552" V 3600 7150 50  0001 C CNN "Digikey"
F 5 "1,51" V 3600 7150 50  0001 C CNN "Cost"
F 6 "R" H 3600 7150 50  0001 C CNN "PN"
	1    3600 7150
	0    1    1    0   
$EndComp
Text GLabel 3750 7050 2    50   Input ~ 0
TH_1_Sense
Text GLabel 3450 6950 0    50   Input ~ 0
GND
Text GLabel 3450 7150 0    50   Input ~ 0
VDD
Wire Wire Line
	3750 6950 3750 7150
Text Notes 3250 6700 0    50   ~ 0
Temperature Measurement \nfor MCU
Wire Notes Line
	4350 6500 4350 7500
Wire Notes Line
	3200 6500 3200 7500
Wire Notes Line
	3200 6500 4350 6500
Wire Notes Line
	3200 7500 4350 7500
Wire Notes Line
	5300 3650 7200 3650
Wire Notes Line
	5300 1750 7200 1750
Wire Notes Line
	2450 1750 4950 1750
Wire Notes Line
	2450 4450 4950 4450
$EndSCHEMATC
