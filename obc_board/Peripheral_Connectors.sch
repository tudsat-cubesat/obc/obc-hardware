EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 9 10
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L obc_board-rescue:Conn_01x06_Male-Connector CAN_?
U 1 1 6051C149
P 5250 2550
AR Path="/6051C149" Ref="CAN_?"  Part="1" 
AR Path="/604E7E81/6051C149" Ref="CAN_?"  Part="1" 
AR Path="/60272770/6051C149" Ref="CAN_1"  Part="1" 
F 0 "CAN_1" H 5358 2931 50  0000 C CNN
F 1 "CAN5" H 5358 2840 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x06_P2.54mm_Horizontal" H 5250 2550 50  0001 C CNN
F 3 "~" H 5250 2550 50  0001 C CNN
F 4 "Ignore" H 5250 2550 50  0001 C CNN "PN"
	1    5250 2550
	1    0    0    -1  
$EndComp
Text GLabel 5450 2650 2    50   Input ~ 0
CAN_2_L
Text GLabel 5450 2550 2    50   Input ~ 0
GND
Text GLabel 5450 2450 2    50   Input ~ 0
CAN_1_L
Text GLabel 5450 2350 2    50   Input ~ 0
CAN_1_H
Text GLabel 5450 2750 2    50   Input ~ 0
CAN_2_H
$Comp
L obc_board-rescue:Conn_01x04-Connector_Generic J?
U 1 1 6051C155
P 7000 2600
AR Path="/6051C155" Ref="J?"  Part="1" 
AR Path="/604E7E81/6051C155" Ref="J?"  Part="1" 
AR Path="/60272770/6051C155" Ref="J3"  Part="1" 
F 0 "J3" H 7000 2200 50  0000 C CNN
F 1 "USART_2" H 7000 2300 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 7000 2600 50  0001 C CNN
F 3 "~" H 7000 2600 50  0001 C CNN
F 4 "Ignore" H 7000 2600 50  0001 C CNN "PN"
	1    7000 2600
	-1   0    0    1   
$EndComp
$Comp
L obc_board-rescue:Conn_01x07_Male-Connector J?
U 1 1 6051C15C
P 5300 4100
AR Path="/6051C15C" Ref="J?"  Part="1" 
AR Path="/604E7E81/6051C15C" Ref="J?"  Part="1" 
AR Path="/60272770/6051C15C" Ref="J4"  Part="1" 
F 0 "J4" H 5400 4600 50  0000 C CNN
F 1 "SPI1" H 5400 4500 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x07_P2.54mm_Vertical" H 5300 4100 50  0001 C CNN
F 3 "~" H 5300 4100 50  0001 C CNN
F 4 "Ignore" H 5300 4100 50  0001 C CNN "PN"
	1    5300 4100
	1    0    0    -1  
$EndComp
$Comp
L obc_board-rescue:R-Device R?
U 1 1 6051C165
P 7850 3950
AR Path="/6051C165" Ref="R?"  Part="1" 
AR Path="/604E7E81/6051C165" Ref="R?"  Part="1" 
AR Path="/60272770/6051C165" Ref="R4"  Part="1" 
F 0 "R4" V 7850 3950 50  0000 C CNN
F 1 "4.7k" V 7950 3950 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 7780 3950 50  0001 C CNN
F 3 "https://d1d2qsbl8m0m72.cloudfront.net/en/products/databook/datasheet/passive/resistor/chip_resistor/esr-e.pdf" H 7850 3950 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/rohm-semiconductor/ESR03EZPJ472/RHM4-7KDCT-ND/1762936" V 7850 3950 50  0001 C CNN "Digikey"
F 5 "0,08" V 7850 3950 50  0001 C CNN "Cost"
F 6 "R" H 7850 3950 50  0001 C CNN "PN"
	1    7850 3950
	0    -1   -1   0   
$EndComp
Text GLabel 8000 3950 2    50   Input ~ 0
VDD
Text GLabel 8000 4250 2    50   Input ~ 0
VDD
Text GLabel 3750 4200 2    50   Input ~ 0
SYS_JTDO_SWO
Text GLabel 3750 4400 2    50   Input ~ 0
SYS_JRST
Text GLabel 3750 4100 2    50   Input ~ 0
SYS_JTMS_SWDIO
Text GLabel 3750 4000 2    50   Input ~ 0
SYS_JTCK_SWCLK
Text GLabel 3750 4300 2    50   Input ~ 0
SYS_JTDI
Text GLabel 7700 4050 2    50   Input ~ 0
I2C_2_SCL
Text GLabel 7700 4150 2    50   Input ~ 0
I2C_2_SDA
Text GLabel 3250 4700 2    50   Input ~ 0
GND
$Comp
L obc_board-rescue:Conn_02x02_Odd_Even-Connector_Generic J?
U 1 1 6051C176
P 7400 4050
AR Path="/6051C176" Ref="J?"  Part="1" 
AR Path="/604E7E81/6051C176" Ref="J?"  Part="1" 
AR Path="/60272770/6051C176" Ref="J2"  Part="1" 
F 0 "J2" H 7550 4150 50  0000 C CNN
F 1 "i2C2" H 7350 4150 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x02_P2.54mm_Vertical" H 7400 4050 50  0001 C CNN
F 3 "~" H 7400 4050 50  0001 C CNN
F 4 "Ignore" H 7400 4050 50  0001 C CNN "PN"
	1    7400 4050
	1    0    0    -1  
$EndComp
Text GLabel 7000 4050 0    50   Input ~ 0
VDD
Text GLabel 7000 4150 0    50   Input ~ 0
GND
Text GLabel 6050 2850 2    50   Input ~ 0
VDD
Text GLabel 6100 4400 2    50   Input ~ 0
VDD
Text GLabel 7800 2700 2    50   Input ~ 0
VDD
Text GLabel 7200 2600 2    50   Input ~ 0
GND
Wire Wire Line
	7700 4150 7700 4250
Wire Wire Line
	7700 4050 7700 3950
Wire Wire Line
	7000 4150 7200 4150
Wire Wire Line
	5500 4400 5900 4400
Wire Wire Line
	7200 2700 7600 2700
Wire Wire Line
	5450 2850 5850 2850
$Comp
L obc_board-rescue:Ferrite_Bead_Small-Device FB?
U 1 1 6051C18B
P 7100 4050
AR Path="/6051C18B" Ref="FB?"  Part="1" 
AR Path="/604E7E81/6051C18B" Ref="FB?"  Part="1" 
AR Path="/60272770/6051C18B" Ref="FB10"  Part="1" 
F 0 "FB10" V 7000 4100 50  0000 C CNN
F 1 "1k/600" V 7250 4200 50  0001 C CNN
F 2 "Inductor_SMD:L_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 7030 4050 50  0001 C CNN
F 3 "https://www.we-online.de/katalog/datasheet/742792662.pdf" H 7100 4050 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/w%C3%BCrth-elektronik/742792662/732-4670-1-ND/4310442" V 7100 4050 50  0001 C CNN "Digikey"
F 5 "0,14" V 7100 4050 50  0001 C CNN "Cost"
F 6 "Ignore" H 7100 4050 50  0001 C CNN "PN"
	1    7100 4050
	0    1    1    0   
$EndComp
$Comp
L obc_board-rescue:Ferrite_Bead_Small-Device FB?
U 1 1 6051C194
P 7700 2700
AR Path="/6051C194" Ref="FB?"  Part="1" 
AR Path="/604E7E81/6051C194" Ref="FB?"  Part="1" 
AR Path="/60272770/6051C194" Ref="FB13"  Part="1" 
F 0 "FB13" V 7600 2800 50  0000 C CNN
F 1 "1k/600" V 7850 2850 50  0001 C CNN
F 2 "Inductor_SMD:L_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 7630 2700 50  0001 C CNN
F 3 "https://www.we-online.de/katalog/datasheet/742792662.pdf" H 7700 2700 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/w%C3%BCrth-elektronik/742792662/732-4670-1-ND/4310442" V 7700 2700 50  0001 C CNN "Digikey"
F 5 "0,14" V 7700 2700 50  0001 C CNN "Cost"
F 6 "Ignore" H 7700 2700 50  0001 C CNN "PN"
	1    7700 2700
	0    1    1    0   
$EndComp
$Comp
L obc_board-rescue:Ferrite_Bead_Small-Device FB?
U 1 1 6051C19D
P 6000 4400
AR Path="/6051C19D" Ref="FB?"  Part="1" 
AR Path="/604E7E81/6051C19D" Ref="FB?"  Part="1" 
AR Path="/60272770/6051C19D" Ref="FB12"  Part="1" 
F 0 "FB12" V 5900 4500 50  0000 C CNN
F 1 "1k/600" V 6150 4550 50  0001 C CNN
F 2 "Inductor_SMD:L_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 5930 4400 50  0001 C CNN
F 3 "https://www.we-online.de/katalog/datasheet/742792662.pdf" H 6000 4400 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/w%C3%BCrth-elektronik/742792662/732-4670-1-ND/4310442" V 6000 4400 50  0001 C CNN "Digikey"
F 5 "0,14" V 6000 4400 50  0001 C CNN "Cost"
F 6 "Ignore" H 6000 4400 50  0001 C CNN "PN"
	1    6000 4400
	0    1    1    0   
$EndComp
$Comp
L obc_board-rescue:Ferrite_Bead_Small-Device FB?
U 1 1 6051C1A6
P 5950 2850
AR Path="/6051C1A6" Ref="FB?"  Part="1" 
AR Path="/604E7E81/6051C1A6" Ref="FB?"  Part="1" 
AR Path="/60272770/6051C1A6" Ref="FB11"  Part="1" 
F 0 "FB11" V 5850 2950 50  0000 C CNN
F 1 "1k/600" V 6100 3000 50  0001 C CNN
F 2 "Inductor_SMD:L_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 5880 2850 50  0001 C CNN
F 3 "https://www.we-online.de/katalog/datasheet/742792662.pdf" H 5950 2850 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/w%C3%BCrth-elektronik/742792662/732-4670-1-ND/4310442" V 5950 2850 50  0001 C CNN "Digikey"
F 5 "0,14" V 5950 2850 50  0001 C CNN "Cost"
F 6 "Ignore" H 5950 2850 50  0001 C CNN "PN"
	1    5950 2850
	0    1    1    0   
$EndComp
$Comp
L obc_board-rescue:R-Device R?
U 1 1 6051C1AF
P 7850 4250
AR Path="/6051C1AF" Ref="R?"  Part="1" 
AR Path="/604E7E81/6051C1AF" Ref="R?"  Part="1" 
AR Path="/60272770/6051C1AF" Ref="R5"  Part="1" 
F 0 "R5" V 7850 4250 50  0000 C CNN
F 1 "4.7k" V 7750 4250 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 7780 4250 50  0001 C CNN
F 3 "https://d1d2qsbl8m0m72.cloudfront.net/en/products/databook/datasheet/passive/resistor/chip_resistor/esr-e.pdf" H 7850 4250 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/rohm-semiconductor/ESR03EZPJ472/RHM4-7KDCT-ND/1762936" V 7850 4250 50  0001 C CNN "Digikey"
F 5 "0,08" V 7850 4250 50  0001 C CNN "Cost"
F 6 "R" H 7850 4250 50  0001 C CNN "PN"
	1    7850 4250
	0    -1   -1   0   
$EndComp
Text GLabel 7300 2500 2    50   Input ~ 0
USART_2_TX
Text GLabel 7300 2400 2    50   Input ~ 0
USART_2_RX
Text GLabel 3750 4500 2    50   Input ~ 0
SYS_JTDO_SWO
Text GLabel 3750 4600 2    50   Input ~ 0
SYS_JTDI
Text GLabel 3750 3800 2    50   Input ~ 0
SYS_JRST
NoConn ~ 3750 4400
NoConn ~ 3750 4500
NoConn ~ 3750 4600
Wire Wire Line
	3150 4700 3250 4700
Text GLabel 5500 4300 2    50   Input ~ 0
GND
Text GLabel 5500 4000 2    50   Input ~ 0
SPI_1_MOSI
Text GLabel 5500 3900 2    50   Input ~ 0
SPI_1_MISO
Text GLabel 5500 3800 2    50   Input ~ 0
SPI_1_SCK
$Comp
L obc_board-rescue:Conn_ARM_JTAG_SWD_10-Connector J?
U 1 1 6051C1C3
P 3250 4100
AR Path="/6051C1C3" Ref="J?"  Part="1" 
AR Path="/604E7E81/6051C1C3" Ref="J?"  Part="1" 
AR Path="/60272770/6051C1C3" Ref="J6"  Part="1" 
F 0 "J6" H 3150 4650 50  0000 R CNN
F 1 "Conn_ARM_JTAG_SWD_10" H 4200 4650 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x05_P2.54mm_Vertical" H 3250 4100 50  0001 C CNN
F 3 "http://infocenter.arm.com/help/topic/com.arm.doc.ddi0314h/DDI0314H_coresight_components_trm.pdf" V 2900 2850 50  0001 C CNN
F 4 "Ignore" H 3250 4100 50  0001 C CNN "PN"
	1    3250 4100
	1    0    0    -1  
$EndComp
Text GLabel 5500 4100 2    50   Input ~ 0
SPI_1_CS_E_0
Text GLabel 5500 4200 2    50   Input ~ 0
SPI_1_CS_E_1
Text GLabel 3350 2700 2    50   Input ~ 0
USB_OTG_FS_DM
Text GLabel 3350 2600 2    50   Input ~ 0
USB_OTG_FS_DP
$Comp
L obc_board-rescue:USB_B_Micro-Connector J?
U 1 1 6051C1D0
P 2950 2600
AR Path="/6051C1D0" Ref="J?"  Part="1" 
AR Path="/604E7E81/6051C1D0" Ref="J?"  Part="1" 
AR Path="/60272770/6051C1D0" Ref="J12"  Part="1" 
F 0 "J12" H 3007 3067 50  0000 C CNN
F 1 "DX4R005JJ2R1800" H 2800 3000 50  0000 C CNN
F 2 "Connector_USB:USB_Micro-B_Molex-105017-0001" H 3100 2550 50  0001 C CNN
F 3 "https://www.jae.com/direct/topics/topics_file_download/topics_id=64360&ext_no=08&_lang=en" H 3100 2550 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/jae-electronics/DX4R005JJ2R1800/670-2675-1-ND/3903235" H 2950 2600 50  0001 C CNN "Digikey"
F 5 "0,7" H 2950 2600 50  0001 C CNN "Cost"
F 6 "X" H 2950 2600 50  0001 C CNN "PN"
	1    2950 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	3250 2600 3350 2600
Wire Wire Line
	3250 2700 3350 2700
Text GLabel 3750 2400 2    50   Input ~ 0
USB_OTG_FS_VBUS
Text GLabel 3350 2800 2    50   Input ~ 0
USB_OTG_FS_ID
Wire Wire Line
	3250 2400 3350 2400
Wire Wire Line
	3250 2800 3350 2800
Text GLabel 3050 3000 2    50   Input ~ 0
GND
Wire Wire Line
	2850 3000 2950 3000
Connection ~ 2950 3000
Wire Wire Line
	2950 3000 3050 3000
Wire Wire Line
	7200 2400 7300 2400
Wire Wire Line
	7200 2500 7300 2500
$Comp
L obc_board-rescue:MBR0520-Diode D?
U 1 1 6051C1E5
P 3500 2250
AR Path="/6051C1E5" Ref="D?"  Part="1" 
AR Path="/604E7E81/6051C1E5" Ref="D?"  Part="1" 
AR Path="/60272770/6051C1E5" Ref="D6"  Part="1" 
F 0 "D6" H 3500 2033 50  0000 C CNN
F 1 "MBR0520" H 3500 2124 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-123" H 3500 2075 50  0001 C CNN
F 3 "http://www.mccsemi.com/up_pdf/MBR0520~MBR0580(SOD123).pdf" H 3500 2250 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/on-semiconductor/MBR0520LT1G/MBR0520LT1GOSCT-ND/917965" H 3500 2250 50  0001 C CNN "Digikey"
F 5 "0,23" H 3500 2250 50  0001 C CNN "Cost"
F 6 "D" H 3500 2250 50  0001 C CNN "PN"
	1    3500 2250
	-1   0    0    1   
$EndComp
Text GLabel 4150 2250 2    50   Input ~ 0
VDD_EXT
Wire Wire Line
	3350 2400 3350 2250
Wire Wire Line
	3650 2250 3750 2250
$Comp
L obc_board-rescue:SolderJumper_2_Open-Jumper JP?
U 1 1 6051C1EF
P 3500 2400
AR Path="/6051C1EF" Ref="JP?"  Part="1" 
AR Path="/604E7E81/6051C1EF" Ref="JP?"  Part="1" 
AR Path="/60272770/6051C1EF" Ref="JP6"  Part="1" 
F 0 "JP6" H 4150 2300 50  0000 C CNN
F 1 "SolderJumper_2_Open" H 3600 2300 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_TrianglePad1.0x1.5mm" H 3500 2400 50  0001 C CNN
F 3 "~" H 3500 2400 50  0001 C CNN
F 4 "Ignore" H 3500 2400 50  0001 C CNN "PN"
	1    3500 2400
	1    0    0    -1  
$EndComp
Connection ~ 3350 2400
Wire Wire Line
	3650 2400 3750 2400
Wire Notes Line
	2450 1950 4550 1950
Wire Notes Line
	4550 1950 4550 3100
Wire Notes Line
	4550 3100 2450 3100
Wire Notes Line
	2450 3100 2450 1950
Text Notes 2450 2050 0    50   ~ 0
USB Micro A 2.0 COM 
$Comp
L obc_board-rescue:SolderJumper_2_Open-Jumper JP?
U 1 1 6051C1FD
P 3900 2250
AR Path="/6051C1FD" Ref="JP?"  Part="1" 
AR Path="/604E7E81/6051C1FD" Ref="JP?"  Part="1" 
AR Path="/60272770/6051C1FD" Ref="JP20"  Part="1" 
F 0 "JP20" H 4250 2400 50  0000 C CNN
F 1 "SolderJumper_2_Open" H 4100 2500 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_TrianglePad1.0x1.5mm" H 3900 2250 50  0001 C CNN
F 3 "~" H 3900 2250 50  0001 C CNN
F 4 "Ignore" H 3900 2250 50  0001 C CNN "PN"
	1    3900 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4050 2250 4150 2250
Text GLabel 3250 3400 1    50   Input ~ 0
VDD
Wire Wire Line
	3250 3500 3250 3400
Text GLabel 3150 5000 0    50   Input ~ 0
RESET
Text GLabel 3250 5000 2    50   Input ~ 0
SYS_JRST
Wire Wire Line
	3150 5000 3250 5000
$EndSCHEMATC
