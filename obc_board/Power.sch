EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 10 10
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text GLabel 5800 3150 2    50   Input ~ 0
GND
Text GLabel 5800 3050 2    50   Input ~ 0
VDD_EXT
$Comp
L obc_board-rescue:Conn_01x02_Male-Connector J5
U 1 1 6055E6B8
P 5600 3050
F 0 "J5" H 5600 3000 50  0000 C CNN
F 1 "Conn_01x02_Male" H 5708 3140 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 5600 3050 50  0001 C CNN
F 3 "~" H 5600 3050 50  0001 C CNN
F 4 "Ignore" H 5600 3050 50  0001 C CNN "PN"
	1    5600 3050
	1    0    0    -1  
$EndComp
Text GLabel 5000 3750 0    50   Input ~ 0
GND
$Comp
L obc_board-rescue:LD1117S33TR_SOT223-Regulator_Linear U4
U 1 1 6055E6C2
P 5350 3450
F 0 "U4" H 5350 3692 50  0000 C CNN
F 1 "LD1117S33C" H 5350 3601 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-223-3_TabPin2" H 5350 3650 50  0001 C CNN
F 3 "http://www.st.com/st-web-ui/static/active/en/resource/technical/document/datasheet/CD00000544.pdf" H 5450 3200 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/stmicroelectronics/LD1117S33TR/497-1242-1-ND/586242" H 5350 3450 50  0001 C CNN "Digikey"
F 5 "0,35" H 5350 3450 50  0001 C CNN "Cost"
F 6 "Ignore" H 5350 3450 50  0001 C CNN "PN"
	1    5350 3450
	1    0    0    -1  
$EndComp
Text GLabel 6300 3450 2    50   Input ~ 0
VDD
Text GLabel 4700 3450 0    50   Input ~ 0
VDD_EXT
Text Notes 4650 3200 0    50   ~ 0
Power\n4.75V - 15V to 3.3V\n800mA MAX
$Comp
L obc_board-rescue:LED-Device D1
U 1 1 6055E6CE
P 5850 3750
F 0 "D1" H 5850 3850 50  0000 C CNN
F 1 "LED-RE" H 6050 3700 50  0000 C CNN
F 2 "LED_SMD:LED_0603_1608Metric" H 5850 3750 50  0001 C CNN
F 3 "https://media.digikey.com/pdf/Data%20Sheets/Rohm%20PDFs/SML-31_Series.pdf" H 5850 3750 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/rohm-semiconductor/SML-311UTT86/511-1304-1-ND/637117" H 5850 3750 50  0001 C CNN "Digikey"
F 5 "0,45" H 5850 3750 50  0001 C CNN "Cost"
F 6 "D" H 5850 3750 50  0001 C CNN "PN"
	1    5850 3750
	1    0    0    -1  
$EndComp
$Comp
L obc_board-rescue:R-Device R6
U 1 1 6055E6D7
P 6000 3600
F 0 "R6" V 6000 3550 50  0000 L CNN
F 1 "680" V 5900 3500 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 5930 3600 50  0001 C CNN
F 3 "https://industrial.panasonic.com/cdbs/www-data/pdf/RDO0000/AOA0000C331.pdf" H 6000 3600 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/panasonic-electronic-components/ERJ-PA3J681V/P680BZCT-ND/5036355" V 6000 3600 50  0001 C CNN "Digikey"
F 5 "0,08" V 6000 3600 50  0001 C CNN "Cost"
F 6 "R" H 6000 3600 50  0001 C CNN "PN"
	1    6000 3600
	-1   0    0    1   
$EndComp
Wire Wire Line
	5000 3650 5000 3750
Wire Wire Line
	5000 3750 5350 3750
Wire Wire Line
	5000 3450 5050 3450
Wire Wire Line
	5350 3750 5700 3750
Wire Wire Line
	5700 3750 5700 3650
Connection ~ 5350 3750
Connection ~ 5700 3750
Wire Notes Line
	6650 2900 6650 3850
Wire Notes Line
	4300 2900 4300 3850
$Comp
L obc_board-rescue:C_Small-Device C16
U 1 1 6055E6E9
P 5000 3550
F 0 "C16" H 5100 3650 50  0000 L CNN
F 1 "100n" H 5100 3550 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 5000 3550 50  0001 C CNN
F 3 "https://www.yageo.com/upload/media/product/productsearch/datasheet/mlcc/UPY-GPHC_X7R_6.3V-to-50V_18.pdf" H 5000 3550 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/yageo/CC0603KPX7R7BB104/311-1335-1-ND/2103119" V 5000 3550 50  0001 C CNN "Digikey"
F 5 "0,08" V 5000 3550 50  0001 C CNN "Cost"
F 6 "C" H 5000 3550 50  0001 C CNN "PN"
	1    5000 3550
	-1   0    0    1   
$EndComp
$Comp
L obc_board-rescue:C_Small-Device C15
U 1 1 6055E6F2
P 5700 3550
F 0 "C15" H 5750 3650 50  0000 L CNN
F 1 "10u" H 5550 3500 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 5700 3550 50  0001 C CNN
F 3 "http://datasheets.avx.com/cx5r.pdf" H 5700 3550 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/avx-corporation/0603ZD106KAT2A/478-10766-1-ND/7536554" V 5700 3550 50  0001 C CNN "Digikey"
F 5 "0,1" V 5700 3550 50  0001 C CNN "Cost"
F 6 "C" H 5700 3550 50  0001 C CNN "PN"
	1    5700 3550
	-1   0    0    1   
$EndComp
Wire Wire Line
	5650 3450 5700 3450
Connection ~ 5700 3450
$Comp
L obc_board-rescue:SolderJumper_2_Open-Jumper JP11
U 1 1 6055E6FB
P 4850 3450
F 0 "JP11" H 4800 3600 50  0000 C CNN
F 1 "SolderJumper_2_Open" H 4900 3550 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_TrianglePad1.0x1.5mm" H 4850 3450 50  0001 C CNN
F 3 "~" H 4850 3450 50  0001 C CNN
F 4 "Ignore" H 4850 3450 50  0001 C CNN "PN"
	1    4850 3450
	1    0    0    -1  
$EndComp
Connection ~ 5000 3450
Wire Wire Line
	5700 3450 6000 3450
$Comp
L obc_board-rescue:SolderJumper_2_Open-Jumper JP13
U 1 1 6055E704
P 6150 3450
F 0 "JP13" H 6150 3650 50  0000 C CNN
F 1 "SolderJumper_2_Open" H 6200 3550 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_TrianglePad1.0x1.5mm" H 6150 3450 50  0001 C CNN
F 3 "~" H 6150 3450 50  0001 C CNN
F 4 "Ignore" H 6150 3450 50  0001 C CNN "PN"
	1    6150 3450
	1    0    0    -1  
$EndComp
Wire Notes Line
	4300 3850 6650 3850
Wire Notes Line
	4300 2900 6650 2900
Connection ~ 6000 3450
$EndSCHEMATC
