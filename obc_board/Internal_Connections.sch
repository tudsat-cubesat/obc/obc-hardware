EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 8 10
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 7050 1950 0    50   ~ 0
Left Over Connections
Text GLabel 3050 1900 0    50   Input ~ 0
C_SPI_1_CS2
Text GLabel 3050 2000 0    50   Input ~ 0
C_SPI_1_CS1
Text GLabel 3050 2100 0    50   Input ~ 0
C_SPI_1_CLK
Text GLabel 3050 2200 0    50   Input ~ 0
C_UART_1_TX
Text GLabel 3050 2300 0    50   Input ~ 0
C_UART_1_RX
Text GLabel 3050 2400 0    50   Input ~ 0
C_GND
Text GLabel 3050 2500 0    50   Input ~ 0
C_UART_RCS_1_TX
Text GLabel 3050 2600 0    50   Input ~ 0
C_UART_RCS_1_RX
Text GLabel 3050 2700 0    50   Input ~ 0
C_I2C_1_SCL
Text GLabel 3050 2800 0    50   Input ~ 0
C_CAN_1_H
Text GLabel 3050 2900 0    50   Input ~ 0
C_SUP_3V3
Text GLabel 3050 3000 0    50   Input ~ 0
C_SUP_3V3
Text GLabel 3050 3100 0    50   Input ~ 0
C_AGND
Text GLabel 3050 3200 0    50   Input ~ 0
C_CPU_WD_1
Text GLabel 3050 3300 0    50   Input ~ 0
C_GLO_KS_1
Text GLabel 3050 3400 0    50   Input ~ 0
C_GLO_KS_3
Text GLabel 6000 2000 2    50   Input ~ 0
C_SPI_1_MOSI
Text GLabel 6000 2100 2    50   Input ~ 0
C_SPI_1_MISO
Text GLabel 6000 2200 2    50   Input ~ 0
C_UART_1_CTS
Text GLabel 6000 2300 2    50   Input ~ 0
C_UART_1_RTS
Text GLabel 6000 2400 2    50   Input ~ 0
C_GND
Text GLabel 6000 2500 2    50   Input ~ 0
C_UART_RCS_1_CTS
Text GLabel 6000 2600 2    50   Input ~ 0
C_UART_RCS_1_RTS
Text GLabel 6000 2700 2    50   Input ~ 0
C_I2C_1_SDA
Text GLabel 6000 2800 2    50   Input ~ 0
C_CAN_1_L
Text GLabel 6000 2900 2    50   Input ~ 0
C_SUP_5V
Text GLabel 6000 3000 2    50   Input ~ 0
C_SUP_5V
Text GLabel 6000 3100 2    50   Input ~ 0
C_SUP_3V3_REF
Text GLabel 6000 3200 2    50   Input ~ 0
C_CPU_WD_2
Text GLabel 6000 3300 2    50   Input ~ 0
C_GLO_KS_2
Text GLabel 6000 3400 2    50   Input ~ 0
C_GLO_KS_4
Text Notes 5450 1950 0    50   ~ 0
PC104 H1 Label Connections
Text Notes 5450 3750 0    50   ~ 0
PC104 H2 Label Connections
Text GLabel 3050 3700 0    50   Input ~ 0
C_SPI_2_CS2
Text GLabel 3050 3800 0    50   Input ~ 0
C_SPI_2_CS1
Text GLabel 3050 3900 0    50   Input ~ 0
C_SPI_2_CLK
Text GLabel 3050 4000 0    50   Input ~ 0
C_UART_2_TX
Text GLabel 3050 4100 0    50   Input ~ 0
C_UART_2_RX
Text GLabel 3050 4200 0    50   Input ~ 0
C_GND
Text GLabel 3050 4300 0    50   Input ~ 0
C_UART_RCS_2_TX
Text GLabel 3050 4400 0    50   Input ~ 0
C_UART_RCS_2_RX
Text GLabel 3050 4500 0    50   Input ~ 0
C_I2C_2_SCL
Text GLabel 3050 4600 0    50   Input ~ 0
C_CAN_2_L
Text GLabel 3050 4700 0    50   Input ~ 0
C_SUP_UNREG
Text GLabel 3050 4800 0    50   Input ~ 0
C_SUP_UNREG
Text GLabel 3050 4900 0    50   Input ~ 0
C_SUP_5V_REF
Text GLabel 3050 5000 0    50   Input ~ 0
C_GLO_SYNC
Text GLabel 3050 5100 0    50   Input ~ 0
C_GLO_KS_5
Text GLabel 3050 5200 0    50   Input ~ 0
C_GLO_KS_7
Text GLabel 6000 3800 2    50   Input ~ 0
C_SPI_2_MOSI
Text GLabel 6000 3900 2    50   Input ~ 0
C_SPI_2_MISO
Text GLabel 6000 4000 2    50   Input ~ 0
C_UART_2_CTS
Text GLabel 6000 4100 2    50   Input ~ 0
C_UART_2_RTS
Text GLabel 6000 4200 2    50   Input ~ 0
C_GND
Text GLabel 6000 4300 2    50   Input ~ 0
C_UART_RCS_2_CTS
Text GLabel 6000 4400 2    50   Input ~ 0
C_UART_RCS_2_RTS
Text GLabel 6000 4500 2    50   Input ~ 0
C_I2C_2_SDA
Text GLabel 6000 4600 2    50   Input ~ 0
C_CAN_2_H
Text GLabel 6000 4800 2    50   Input ~ 0
C_SUP_BAT_2
Text GLabel 6000 4900 2    50   Input ~ 0
C_CPU_SELECT
Text GLabel 6000 5000 2    50   Input ~ 0
C_GLO_FAULT
Text GLabel 6000 5100 2    50   Input ~ 0
C_GLO_KS_6
Text GLabel 6000 5200 2    50   Input ~ 0
C_GLO_KS_8
Wire Wire Line
	6000 3000 5900 3000
Wire Wire Line
	3050 2900 3150 2900
Wire Wire Line
	3050 3000 3150 3000
Text GLabel 3650 2950 2    50   Input ~ 0
VDD
Text GLabel 5400 2950 0    50   Input ~ 0
VDD5
Text GLabel 4050 2900 2    50   Input ~ 0
AGND
Text GLabel 4950 2900 0    50   Input ~ 0
REF_3V3
Text GLabel 3150 4900 2    50   Input ~ 0
REF_5V
NoConn ~ 3050 3400
NoConn ~ 3050 3300
NoConn ~ 3050 3200
NoConn ~ 6000 3400
NoConn ~ 6000 3300
NoConn ~ 6000 3200
NoConn ~ 6000 5200
Text GLabel 3150 2000 2    50   Input ~ 0
SPI_3_CS_1
Text GLabel 3150 1900 2    50   Input ~ 0
SPI_3_CS_2
Text GLabel 5900 2000 0    50   Input ~ 0
SPI_3_MOSI
Text GLabel 5900 2100 0    50   Input ~ 0
SPI_3_MISO
Text GLabel 3150 2100 2    50   Input ~ 0
SPI_3_SCK
Text GLabel 3150 3800 2    50   Input ~ 0
SPI_1_CS_1
Text GLabel 3150 3700 2    50   Input ~ 0
SPI_1_CS_2
Text GLabel 3150 3900 2    50   Input ~ 0
SPI_1_SCK
Text GLabel 5900 3800 0    50   Input ~ 0
SPI_1_MISO
Text GLabel 5900 3900 0    50   Input ~ 0
SPI_1_MOSI
Wire Wire Line
	5900 3800 6000 3800
Wire Wire Line
	5900 3900 6000 3900
Wire Wire Line
	3050 3700 3150 3700
Wire Wire Line
	3050 3800 3150 3800
Wire Wire Line
	3050 3900 3150 3900
Wire Wire Line
	5900 2000 6000 2000
Wire Wire Line
	5900 2100 6000 2100
Text GLabel 3150 2200 2    50   Input ~ 0
UART_4_RX
Text GLabel 3150 2300 2    50   Input ~ 0
UART_4_TX
Text GLabel 3150 4600 2    50   Input ~ 0
CAN_2_L
Text GLabel 5900 2800 0    50   Input ~ 0
CAN_1_L
Text GLabel 3150 2800 2    50   Input ~ 0
CAN_1_H
Text GLabel 5900 4600 0    50   Input ~ 0
CAN_2_H
Wire Wire Line
	3050 2800 3150 2800
Wire Wire Line
	5900 2800 6000 2800
Wire Wire Line
	3050 4600 3150 4600
Wire Wire Line
	5900 4600 6000 4600
Text GLabel 3150 2700 2    50   Input ~ 0
I2C_2_SCL
Text GLabel 5900 2700 0    50   Input ~ 0
I2C_2_SDA
Wire Wire Line
	3050 2700 3150 2700
Wire Wire Line
	5900 2700 6000 2700
NoConn ~ 6000 2200
NoConn ~ 6000 2300
NoConn ~ 6000 2500
NoConn ~ 6000 2600
NoConn ~ 3050 2500
NoConn ~ 3050 2600
NoConn ~ 3050 4000
NoConn ~ 3050 4100
NoConn ~ 3050 4300
NoConn ~ 3050 4400
NoConn ~ 6000 4000
NoConn ~ 6000 4100
NoConn ~ 6000 4300
NoConn ~ 6000 4400
Wire Wire Line
	3050 1900 3150 1900
Wire Wire Line
	3050 2000 3150 2000
Wire Wire Line
	3050 2100 3150 2100
Wire Wire Line
	3050 2200 3150 2200
Wire Wire Line
	3050 2300 3150 2300
Text GLabel 5900 4500 0    50   Input ~ 0
PCDU_5V
Text GLabel 5850 4900 0    50   Input ~ 0
PCDU_S0
Text GLabel 5850 5000 0    50   Input ~ 0
PCDU_S1
Text GLabel 5850 5100 0    50   Input ~ 0
PCDU_S2
Text GLabel 3150 4500 2    50   Input ~ 0
PCDU_5V
Text GLabel 3150 4700 2    50   Input ~ 0
PCDU_GND
Text GLabel 5900 4700 0    50   Input ~ 0
PCDU_GND
Text GLabel 5900 4800 0    50   Input ~ 0
PCDU_GND
Text GLabel 3150 5000 2    50   Input ~ 0
PCDU_S0
Text GLabel 3150 5100 2    50   Input ~ 0
PCDU_S1
Text GLabel 3150 5200 2    50   Input ~ 0
PCDU_S2
Text GLabel 6000 4700 2    50   Input ~ 0
C_SUP_BAT_1
Text GLabel 3150 4800 2    50   Input ~ 0
PCDU_GND
Wire Wire Line
	5900 4700 6000 4700
Wire Wire Line
	5900 4800 6000 4800
Wire Wire Line
	5850 4900 6000 4900
Wire Wire Line
	5850 5000 6000 5000
Wire Wire Line
	5850 5100 6000 5100
Wire Wire Line
	3050 4500 3150 4500
Wire Wire Line
	3050 4700 3150 4700
Wire Wire Line
	3050 4800 3150 4800
Wire Wire Line
	3050 5000 3150 5000
Wire Wire Line
	3050 5100 3150 5100
Wire Wire Line
	3050 5200 3150 5200
Wire Wire Line
	5900 4500 6000 4500
Text GLabel 4450 4350 0    50   Input ~ 0
PCDU_GND
Text GLabel 4750 4350 2    50   Input ~ 0
GND
Text GLabel 4450 4650 0    50   Input ~ 0
PCDU_5V
Text GLabel 4750 4650 2    50   Input ~ 0
VDD_EXT
Text GLabel 5150 3300 2    50   Input ~ 0
PCDU_GND
Text GLabel 3850 3300 0    50   Input ~ 0
PCDU_GND
Wire Notes Line
	2300 1800 2300 3500
Wire Notes Line
	2300 3500 6800 3500
Wire Notes Line
	6800 3500 6800 1800
Wire Notes Line
	6800 1800 2300 1800
Wire Notes Line
	2300 3600 2300 5300
Wire Notes Line
	2300 5300 6800 5300
Wire Notes Line
	6800 5300 6800 3600
Wire Notes Line
	6800 3600 2300 3600
$Comp
L obc_board-rescue:Ferrite_Bead_Small-Device FB?
U 1 1 60575679
P 3550 2950
AR Path="/60575679" Ref="FB?"  Part="1" 
AR Path="/60271867/60575679" Ref="FB18"  Part="1" 
F 0 "FB18" V 3450 3100 50  0000 C CNN
F 1 "1k/600" V 3700 3100 50  0001 C CNN
F 2 "Inductor_SMD:L_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 3480 2950 50  0001 C CNN
F 3 "https://www.we-online.de/katalog/datasheet/742792662.pdf" H 3550 2950 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/w%C3%BCrth-elektronik/742792662/732-4670-1-ND/4310442" V 3550 2950 50  0001 C CNN "Digikey"
F 5 "0,14" V 3550 2950 50  0001 C CNN "Cost"
F 6 "Ignore" H 3550 2950 50  0001 C CNN "PN"
	1    3550 2950
	0    1    1    0   
$EndComp
$Comp
L obc_board-rescue:Ferrite_Bead_Small-Device FB?
U 1 1 60575682
P 5500 2950
AR Path="/60575682" Ref="FB?"  Part="1" 
AR Path="/60271867/60575682" Ref="FB19"  Part="1" 
F 0 "FB19" V 5400 2750 50  0000 C CNN
F 1 "1k/600" V 5650 3100 50  0001 C CNN
F 2 "Inductor_SMD:L_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 5430 2950 50  0001 C CNN
F 3 "https://www.we-online.de/katalog/datasheet/742792662.pdf" H 5500 2950 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/w%C3%BCrth-elektronik/742792662/732-4670-1-ND/4310442" V 5500 2950 50  0001 C CNN "Digikey"
F 5 "0,14" V 5500 2950 50  0001 C CNN "Cost"
F 6 "Ignore" H 5500 2950 50  0001 C CNN "PN"
	1    5500 2950
	0    1    1    0   
$EndComp
Text GLabel 7650 3550 0    50   Input ~ 0
GPIO_17
Text GLabel 7650 3150 0    50   Input ~ 0
GPIO_13
Text GLabel 7650 3250 0    50   Input ~ 0
GPIO_14
Text GLabel 7650 3450 0    50   Input ~ 0
GPIO_16
Text GLabel 7650 2550 0    50   Input ~ 0
GPIO_07
Text GLabel 7650 2850 0    50   Input ~ 0
GPIO_10
Text GLabel 7650 2750 0    50   Input ~ 0
GPIO_09
Text GLabel 7650 2650 0    50   Input ~ 0
GPIO_08
Text GLabel 7650 3950 0    50   Input ~ 0
SPI_3_CS_0
Text GLabel 7650 2450 0    50   Input ~ 0
GPIO_06
Text GLabel 4500 5050 0    50   Input ~ 0
GPIO_25
Text GLabel 4500 5150 0    50   Input ~ 0
GPIO_26
Text GLabel 4500 4950 0    50   Input ~ 0
GPIO_24
Text GLabel 7650 4050 0    50   Input ~ 0
SPI_3_CS_3
Text GLabel 7650 2950 0    50   Input ~ 0
GPIO_11
Text GLabel 7650 3050 0    50   Input ~ 0
GPIO_12
Text GLabel 7650 3350 0    50   Input ~ 0
GPIO_15
NoConn ~ 7650 3550
NoConn ~ 7650 3450
NoConn ~ 7650 3350
NoConn ~ 7650 3250
NoConn ~ 7650 3150
NoConn ~ 7650 3050
NoConn ~ 7650 2950
NoConn ~ 7650 2850
NoConn ~ 7650 2750
NoConn ~ 7650 2650
NoConn ~ 7650 2550
NoConn ~ 7650 2450
Wire Notes Line
	6900 1800 8000 1800
Wire Wire Line
	6000 2900 5900 2900
Text GLabel 3150 4200 2    50   Input ~ 0
GND
Wire Wire Line
	3050 4200 3150 4200
NoConn ~ 6000 2400
NoConn ~ 3050 2400
$Comp
L obc_board-rescue:SolderJumper_3_Open-Jumper JP?
U 1 1 605756AC
P 3950 3100
AR Path="/605756AC" Ref="JP?"  Part="1" 
AR Path="/60271867/605756AC" Ref="JP2"  Part="1" 
F 0 "JP2" V 3904 3168 50  0000 L CNN
F 1 "SolderJumper_3_Open" V 4250 2550 50  0000 L CNN
F 2 "Jumper:SolderJumper-3_P2.0mm_Open_TrianglePad1.0x1.5mm" H 3950 3100 50  0001 C CNN
F 3 "~" H 3950 3100 50  0001 C CNN
F 4 "Ignore" H 3950 3100 50  0001 C CNN "PN"
	1    3950 3100
	0    1    1    0   
$EndComp
$Comp
L obc_board-rescue:SolderJumper_3_Open-Jumper JP?
U 1 1 605756B3
P 5050 3100
AR Path="/605756B3" Ref="JP?"  Part="1" 
AR Path="/60271867/605756B3" Ref="JP5"  Part="1" 
F 0 "JP5" V 5096 3167 50  0000 L CNN
F 1 "SolderJumper_3_Open" V 4750 2550 50  0000 L CNN
F 2 "Jumper:SolderJumper-3_P2.0mm_Open_TrianglePad1.0x1.5mm" H 5050 3100 50  0001 C CNN
F 3 "~" H 5050 3100 50  0001 C CNN
F 4 "Ignore" H 5050 3100 50  0001 C CNN "PN"
	1    5050 3100
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3950 2900 4050 2900
Wire Wire Line
	4950 2900 5050 2900
$Comp
L obc_board-rescue:SolderJumper_2_Open-Jumper JP?
U 1 1 605756BC
P 4600 4650
AR Path="/605756BC" Ref="JP?"  Part="1" 
AR Path="/60271867/605756BC" Ref="JP4"  Part="1" 
F 0 "JP4" H 4600 4855 50  0000 C CNN
F 1 "SolderJumper_2_Open" H 4600 4764 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_TrianglePad1.0x1.5mm" H 4600 4650 50  0001 C CNN
F 3 "~" H 4600 4650 50  0001 C CNN
F 4 "Ignore" H 4600 4650 50  0001 C CNN "PN"
	1    4600 4650
	1    0    0    -1  
$EndComp
$Comp
L obc_board-rescue:SolderJumper_2_Open-Jumper JP?
U 1 1 605756C3
P 4600 4350
AR Path="/605756C3" Ref="JP?"  Part="1" 
AR Path="/60271867/605756C3" Ref="JP3"  Part="1" 
F 0 "JP3" H 4600 4555 50  0000 C CNN
F 1 "SolderJumper_2_Open" H 4600 4464 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_TrianglePad1.0x1.5mm" H 4600 4350 50  0001 C CNN
F 3 "~" H 4600 4350 50  0001 C CNN
F 4 "Ignore" H 4600 4350 50  0001 C CNN "PN"
	1    4600 4350
	1    0    0    -1  
$EndComp
Text GLabel 5900 4200 0    50   Input ~ 0
GND
Wire Wire Line
	5900 4200 6000 4200
Text GLabel 4600 4950 2    50   Input ~ 0
PCDU_S0
Text GLabel 4600 5050 2    50   Input ~ 0
PCDU_S1
Text GLabel 4600 5150 2    50   Input ~ 0
PCDU_S2
Text GLabel 7650 2350 0    50   Input ~ 0
GPIO_04
Wire Wire Line
	4500 4950 4600 4950
Wire Wire Line
	4500 5050 4600 5050
Wire Wire Line
	4500 5150 4600 5150
Wire Wire Line
	3050 4900 3150 4900
NoConn ~ 7650 2350
$Comp
L obc_board-rescue:SolderJumper_2_Open-Jumper JP?
U 1 1 605756D5
P 3300 2950
AR Path="/605756D5" Ref="JP?"  Part="1" 
AR Path="/60271867/605756D5" Ref="JP1"  Part="1" 
F 0 "JP1" H 3250 3000 50  0000 C CNN
F 1 "SolderJumper_2_Open" H 3450 2850 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_TrianglePad1.0x1.5mm" H 3300 2950 50  0001 C CNN
F 3 "~" H 3300 2950 50  0001 C CNN
F 4 "Ignore" H 3300 2950 50  0001 C CNN "PN"
	1    3300 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 2900 3150 2950
Connection ~ 3150 2950
Wire Wire Line
	3150 2950 3150 3000
Wire Wire Line
	3050 3100 3800 3100
Wire Wire Line
	5050 3300 5150 3300
Wire Wire Line
	3950 3300 3850 3300
$Comp
L obc_board-rescue:SolderJumper_2_Open-Jumper JP?
U 1 1 605756E2
P 5750 2950
AR Path="/605756E2" Ref="JP?"  Part="1" 
AR Path="/60271867/605756E2" Ref="JP7"  Part="1" 
F 0 "JP7" H 5700 3000 50  0000 C CNN
F 1 "SolderJumper_2_Open" H 5600 2850 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_TrianglePad1.0x1.5mm" H 5750 2950 50  0001 C CNN
F 3 "~" H 5750 2950 50  0001 C CNN
F 4 "Ignore" H 5750 2950 50  0001 C CNN "PN"
	1    5750 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	5900 2900 5900 2950
Connection ~ 5900 2950
Wire Wire Line
	5900 2950 5900 3000
Wire Wire Line
	5200 3100 6000 3100
NoConn ~ 7650 3950
NoConn ~ 7650 4050
$Comp
L obc_board-rescue:SolderJumper_2_Open-Jumper JP?
U 1 1 605756EE
P 7400 5150
AR Path="/605756EE" Ref="JP?"  Part="1" 
AR Path="/60271867/605756EE" Ref="JP16"  Part="1" 
F 0 "JP16" H 7400 5355 50  0000 C CNN
F 1 "PCDU Pinout?" H 7400 5264 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_Pad1.0x1.5mm" H 7400 5150 50  0001 C CNN
F 3 "~" H 7400 5150 50  0001 C CNN
	1    7400 5150
	1    0    0    -1  
$EndComp
NoConn ~ 7550 5150
NoConn ~ 7250 5150
Wire Notes Line
	6900 4850 6900 1800
Wire Notes Line
	8000 1800 8000 4850
Wire Notes Line
	8000 4850 6900 4850
Text GLabel 10350 4350 2    50   Input ~ 0
ADC_1_IN_1
Text GLabel 10350 4450 2    50   Input ~ 0
ADC_2_IN_2
Text GLabel 10350 4550 2    50   Input ~ 0
ADC_3_IN_3
Text GLabel 10250 4350 0    50   Input ~ 0
TH_1_Sense
Wire Wire Line
	10350 4350 10250 4350
Text Notes 9900 2900 0    50   ~ 0
Mounting Holes
Wire Notes Line
	9700 2800 10850 2800
Wire Notes Line
	10850 2800 10850 3800
Wire Notes Line
	10850 3800 9700 3800
Wire Notes Line
	9700 3800 9700 2800
Text GLabel 10250 4450 0    50   Input ~ 0
A_Com
Wire Wire Line
	10350 4450 10250 4450
Wire Wire Line
	10350 4550 10250 4550
Text Notes 10050 4250 0    50   ~ 0
ADC Usage
Wire Notes Line
	10850 4150 10850 4750
Wire Notes Line
	9750 4750 9750 4150
Wire Notes Line
	9750 4150 10850 4150
Wire Notes Line
	9750 4750 10850 4750
Text GLabel 10250 4550 0    50   Input ~ 0
EXP_Sense
$Comp
L obc_board-rescue:MountingHole_Pad-Mechanical H_UL1
U 1 1 6058B3FD
P 9900 3050
F 0 "H_UL1" V 9950 3250 50  0000 L CNN
F 1 "MountingHole_Pad" V 9850 3250 50  0000 L CNN
F 2 "MountingHole:MountingHole_3mm_Pad_Via" H 9900 3050 50  0001 C CNN
F 3 "~" H 9900 3050 50  0001 C CNN
F 4 "Ignore" H 9900 3050 50  0001 C CNN "PN"
	1    9900 3050
	0    1    1    0   
$EndComp
$Comp
L obc_board-rescue:MountingHole_Pad-Mechanical H_UR1
U 1 1 6058B412
P 9900 3250
F 0 "H_UR1" V 9950 3450 50  0000 L CNN
F 1 "MountingHole_Pad" V 9850 3450 50  0000 L CNN
F 2 "MountingHole:MountingHole_3mm_Pad_Via" H 9900 3250 50  0001 C CNN
F 3 "~" H 9900 3250 50  0001 C CNN
F 4 "Ignore" H 9900 3250 50  0001 C CNN "PN"
	1    9900 3250
	0    1    1    0   
$EndComp
$Comp
L obc_board-rescue:MountingHole_Pad-Mechanical H_LR1
U 1 1 6058B40B
P 9900 3650
F 0 "H_LR1" V 9950 3850 50  0000 L CNN
F 1 "MountingHole_Pad" V 9850 3850 50  0000 L CNN
F 2 "MountingHole:MountingHole_3mm_Pad_Via" H 9900 3650 50  0001 C CNN
F 3 "~" H 9900 3650 50  0001 C CNN
F 4 "Ignore" H 9900 3650 50  0001 C CNN "PN"
	1    9900 3650
	0    1    1    0   
$EndComp
$Comp
L obc_board-rescue:MountingHole_Pad-Mechanical H_LL1
U 1 1 6058B404
P 9900 3450
F 0 "H_LL1" V 9950 3650 50  0000 L CNN
F 1 "MountingHole_Pad" V 9850 3650 50  0000 L CNN
F 2 "MountingHole:MountingHole_3mm_Pad_Via" H 9900 3450 50  0001 C CNN
F 3 "~" H 9900 3450 50  0001 C CNN
F 4 "Ignore" H 9900 3450 50  0001 C CNN "PN"
	1    9900 3450
	0    1    1    0   
$EndComp
$EndSCHEMATC
