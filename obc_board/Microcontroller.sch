EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 10
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L obc_board-rescue:STM32L496VGTx-MCU_ST_STM32L4 U2
U 1 1 60383365
P 2350 4700
F 0 "U2" H 2350 1811 50  0000 C CNN
F 1 "STM32L496VGT3" H 2350 1720 50  0000 C CNN
F 2 "Package_QFP:LQFP-100_14x14mm_P0.5mm" H 1650 2100 50  0001 R CNN
F 3 "http://www.st.com/st-web-ui/static/active/en/resource/technical/document/datasheet/DM00284211.pdf" H 2350 4700 50  0001 C CNN
F 4 "https://www.digikey.de/products/de?keywords=STM32L496VGT3" H 2350 4700 50  0001 C CNN "Digikey"
F 5 "9,22" H 2350 4700 50  0001 C CNN "Cost"
F 6 "U" H 2350 4700 50  0001 C CNN "PN"
	1    2350 4700
	1    0    0    -1  
$EndComp
Text GLabel 1450 4200 0    50   Input ~ 0
AS_Enable
Text GLabel 1450 4300 0    50   Input ~ 0
AS_3
Text GLabel 1450 4400 0    50   Input ~ 0
AS_2
Text GLabel 1450 4500 0    50   Input ~ 0
AS_1
Text GLabel 1450 4600 0    50   Input ~ 0
AS_0
Text GLabel 3250 7000 2    50   Input ~ 0
GPIO_10
Text GLabel 3250 7100 2    50   Input ~ 0
GPIO_09
Text GLabel 3250 7200 2    50   Input ~ 0
GPIO_08
Text GLabel 1450 3600 0    50   Input ~ 0
RCC_OSC_IN
Text GLabel 1450 3700 0    50   Input ~ 0
RCC_OSC_OUT
Text GLabel 3250 5700 2    50   Input ~ 0
ADC_1_IN_1
Text GLabel 3250 5800 2    50   Input ~ 0
ADC_2_IN_2
Text GLabel 3250 5900 2    50   Input ~ 0
ADC_3_IN_3
Text GLabel 3250 6000 2    50   Input ~ 0
GPIO_07
Text GLabel 3250 2300 2    50   Input ~ 0
UART_4_TX
Text GLabel 3250 2400 2    50   Input ~ 0
UART_4_RX
Text GLabel 3250 2500 2    50   Input ~ 0
GPIO_06
Text GLabel 3250 2700 2    50   Input ~ 0
GPIO_04
Text GLabel 3250 2900 2    50   Input ~ 0
GPIO_02
Text GLabel 3250 6100 2    50   Input ~ 0
GPIO_00
Text GLabel 1450 5200 0    50   Input ~ 0
SPI_1_CS_0
Text GLabel 1450 5100 0    50   Input ~ 0
SPI_1_CS_1
Text GLabel 1450 5000 0    50   Input ~ 0
SPI_1_CS_2
Text GLabel 1450 4900 0    50   Input ~ 0
SPI_1_CS_3
Text GLabel 1450 4800 0    50   Input ~ 0
SPI_1_CS_4
Text GLabel 1450 4700 0    50   Input ~ 0
SPI_1_CS_5
Text GLabel 3250 4200 2    50   Input ~ 0
SPI_1_CS_6
Text GLabel 3250 4100 2    50   Input ~ 0
SPI_1_CS_7
Text GLabel 3250 4000 2    50   Input ~ 0
SPI_1_CS_8
Text GLabel 3250 6200 2    50   Input ~ 0
SPI_1_CS_9
Text GLabel 1450 5300 0    50   Input ~ 0
SPI_1_SCK
Text GLabel 1450 5400 0    50   Input ~ 0
SPI_1_MISO
Text GLabel 1450 5500 0    50   Input ~ 0
SPI_1_MOSI
Text GLabel 3250 5000 2    50   Input ~ 0
I2C_2_SCL
Text GLabel 3250 5100 2    50   Input ~ 0
I2C_2_SDA
Text GLabel 3250 5200 2    50   Input ~ 0
LED_Green
Text GLabel 3250 5300 2    50   Input ~ 0
LED_Blue
Text GLabel 3250 5400 2    50   Input ~ 0
LED_Red
Text GLabel 3250 5500 2    50   Input ~ 0
LED_White
Text GLabel 1450 6600 0    50   Input ~ 0
USART_3_RX
Text GLabel 1450 6500 0    50   Input ~ 0
USART_3_TX
Text GLabel 1450 6800 0    50   Input ~ 0
GPIO_25
Text GLabel 1450 6700 0    50   Input ~ 0
GPIO_26
Text GLabel 1450 6900 0    50   Input ~ 0
I2C_4_SCL
Text GLabel 1450 7000 0    50   Input ~ 0
I2C_4_SDA
Text GLabel 3250 6500 2    50   Input ~ 0
GPIO_20
Text GLabel 3250 6400 2    50   Input ~ 0
GPIO_21
Text GLabel 3250 6300 2    50   Input ~ 0
GPIO_22
Text GLabel 1450 7200 0    50   Input ~ 0
GPIO_23
Text GLabel 1450 7100 0    50   Input ~ 0
GPIO_24
Text GLabel 3250 3600 2    50   Input ~ 0
SYS_JTMS_SWDIO
Text GLabel 3250 3700 2    50   Input ~ 0
SYS_JTCK_SWCLK
Text GLabel 3250 3800 2    50   Input ~ 0
SPI_3_CS_0
Text GLabel 3250 6700 2    50   Input ~ 0
SPI_3_SCK
Text GLabel 3250 6800 2    50   Input ~ 0
SPI_3_MISO
Text GLabel 3250 6900 2    50   Input ~ 0
SPI_3_MOSI
Text GLabel 1450 5700 0    50   Input ~ 0
CAN_1_RX
Text GLabel 1450 5800 0    50   Input ~ 0
CAN_1_TX
Text GLabel 1450 5900 0    50   Input ~ 0
SPI_3_CS_1
Text GLabel 1450 6000 0    50   Input ~ 0
SPI_3_CS_2
Text GLabel 1450 6100 0    50   Input ~ 0
SPI_3_CS_3
Text GLabel 1450 6200 0    50   Input ~ 0
USART_2_TX
Text GLabel 1450 6300 0    50   Input ~ 0
USART_2_RX
Text GLabel 1450 4100 0    50   Input ~ 0
GPIO_11
Text GLabel 1450 4000 0    50   Input ~ 0
GPIO_12
Text GLabel 3250 4900 2    50   Input ~ 0
GPIO_13
Text GLabel 3250 4800 2    50   Input ~ 0
GPIO_14
Text GLabel 1450 3800 0    50   Input ~ 0
GPIO_15
Text GLabel 3250 4700 2    50   Input ~ 0
GPIO_16
Text GLabel 3250 4400 2    50   Input ~ 0
GPIO_17
Text GLabel 3250 4600 2    50   Input ~ 0
CAN_2_TX
Text GLabel 3250 4500 2    50   Input ~ 0
CAN_2_RX
$Comp
L obc_board-rescue:C_Small-Device C24
U 1 1 603833B6
P 2900 1700
F 0 "C24" V 2900 1450 50  0000 L CNN
F 1 "10n" V 3000 1600 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 2900 1700 50  0001 C CNN
F 3 "https://www.yageo.com/upload/media/product/productsearch/datasheet/mlcc/UPY-GPHC_X7R_6.3V-to-50V_18.pdf" H 2900 1700 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/yageo/CC0603KPX7R9BB103/311-1572-1-ND/4340579" V 2900 1700 50  0001 C CNN "Digikey"
F 5 "0,08" V 2900 1700 50  0001 C CNN "Cost"
F 6 "C" H 2900 1700 50  0001 C CNN "PN"
	1    2900 1700
	0    1    1    0   
$EndComp
$Comp
L obc_board-rescue:C_Small-Device C11
U 1 1 603833BF
P 2900 1550
F 0 "C11" V 2900 1300 50  0000 L CNN
F 1 "1u" V 2800 1500 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 2900 1550 50  0001 C CNN
F 3 "http://weblib.samsungsem.com/mlcc/mlcc-ec-data-sheet.do?partNumber=CL10A105KP8NNN" H 2900 1550 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/samsung-electro-mechanics/CL10A105KP8NNNC/1276-1182-1-ND/3889268" V 2900 1550 50  0001 C CNN "Digikey"
F 5 "0,08" V 2900 1550 50  0001 C CNN "Cost"
F 6 "C" H 2900 1550 50  0001 C CNN "PN"
	1    2900 1550
	0    1    1    0   
$EndComp
Text GLabel 2800 1350 1    50   Input ~ 0
VDD_UC
Text GLabel 1700 850  0    50   Input ~ 0
VDD_UC
Text GLabel 3200 1350 1    50   Input ~ 0
VDD_UC
Wire Wire Line
	2800 1700 2800 1550
Wire Wire Line
	3000 1550 3000 1700
$Comp
L obc_board-rescue:C_Small-Device C4
U 1 1 603833CD
P 1100 1850
F 0 "C4" V 1150 1900 50  0000 L CNN
F 1 "10u" V 1150 1600 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 1100 1850 50  0001 C CNN
F 3 "http://datasheets.avx.com/cx5r.pdf" H 1100 1850 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/avx-corporation/0603ZD106KAT2A/478-10766-1-ND/7536554" V 1100 1850 50  0001 C CNN "Digikey"
F 5 "0,1" V 1100 1850 50  0001 C CNN "Cost"
F 6 "C" H 1100 1850 50  0001 C CNN "PN"
	1    1100 1850
	0    -1   -1   0   
$EndComp
$Comp
L obc_board-rescue:C_Small-Device C3
U 1 1 603833D6
P 1100 2000
F 0 "C3" V 1150 2050 50  0000 L CNN
F 1 "100n" V 1150 1750 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 1100 2000 50  0001 C CNN
F 3 "https://www.yageo.com/upload/media/product/productsearch/datasheet/mlcc/UPY-GPHC_X7R_6.3V-to-50V_18.pdf" H 1100 2000 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/yageo/CC0603KPX7R7BB104/311-1335-1-ND/2103119" V 1100 2000 50  0001 C CNN "Digikey"
F 5 "0,08" V 1100 2000 50  0001 C CNN "Cost"
F 6 "C" H 1100 2000 50  0001 C CNN "PN"
	1    1100 2000
	0    -1   -1   0   
$EndComp
Text GLabel 3000 1350 1    50   Input ~ 0
GND
Text GLabel 1000 1350 1    50   Input ~ 0
GND
Text Notes 1650 700  0    50   ~ 0
Microcontroller
Wire Wire Line
	2800 1850 2800 1700
Connection ~ 2800 1700
Text GLabel 2000 850  2    50   Input ~ 0
GND
Connection ~ 3000 1550
Wire Wire Line
	3000 1550 3000 1350
Wire Wire Line
	2650 2000 2650 1850
Wire Wire Line
	2650 1850 2800 1850
Wire Wire Line
	2750 2000 3200 2000
$Comp
L obc_board-rescue:Ferrite_Bead_Small-Device FB4
U 1 1 603833EA
P 1850 1700
F 0 "FB4" V 1800 1600 50  0000 C CNN
F 1 "1k/600" V 2000 1850 50  0001 C CNN
F 2 "Inductor_SMD:L_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 1780 1700 50  0001 C CNN
F 3 "https://www.we-online.de/katalog/datasheet/742792662.pdf" H 1850 1700 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/w%C3%BCrth-elektronik/742792662/732-4670-1-ND/4310442" V 1850 1700 50  0001 C CNN "Digikey"
F 5 "0,14" V 1850 1700 50  0001 C CNN "Cost"
F 6 "Ignore" H 1850 1700 50  0001 C CNN "PN"
	1    1850 1700
	0    1    1    0   
$EndComp
Wire Wire Line
	2150 2000 2050 2000
Connection ~ 2050 2000
Text GLabel 1450 2300 0    50   Input ~ 0
RESET
Text GLabel 1450 2600 0    50   Input ~ 0
GND
Text GLabel 1450 2500 0    50   Input ~ 0
VDD_UC
Wire Wire Line
	2150 7500 2250 7500
Connection ~ 2250 7500
Wire Wire Line
	2250 7500 2350 7500
Connection ~ 2350 7500
Wire Wire Line
	2350 7500 2450 7500
Connection ~ 2450 7500
Wire Wire Line
	2450 7500 2550 7500
Connection ~ 2550 7500
Wire Wire Line
	2550 7500 2650 7500
Connection ~ 2650 7500
Wire Wire Line
	2650 7500 3050 7500
Text GLabel 3050 7500 2    50   Input ~ 0
GND
Wire Notes Line
	4000 550  4000 7750
Wire Notes Line
	4000 7750 850  7750
Wire Notes Line
	850  7750 850  550 
Wire Notes Line
	850  550  4000 550 
$Comp
L obc_board-rescue:Ferrite_Bead_Small-Device FB5
U 1 1 60383408
P 1850 1100
F 0 "FB5" V 1800 1000 50  0000 C CNN
F 1 "1k/600" V 2000 1250 50  0001 C CNN
F 2 "Inductor_SMD:L_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 1780 1100 50  0001 C CNN
F 3 "https://www.we-online.de/katalog/datasheet/742792662.pdf" H 1850 1100 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/w%C3%BCrth-elektronik/742792662/732-4670-1-ND/4310442" V 1850 1100 50  0001 C CNN "Digikey"
F 5 "0,14" V 1850 1100 50  0001 C CNN "Cost"
F 6 "Ignore" H 1850 1100 50  0001 C CNN "PN"
	1    1850 1100
	0    1    1    0   
$EndComp
$Comp
L obc_board-rescue:Ferrite_Bead_Small-Device FB8
U 1 1 60383411
P 1850 1400
F 0 "FB8" V 1800 1300 50  0000 C CNN
F 1 "1k/600" V 2000 1550 50  0001 C CNN
F 2 "Inductor_SMD:L_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 1780 1400 50  0001 C CNN
F 3 "https://www.we-online.de/katalog/datasheet/742792662.pdf" H 1850 1400 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/w%C3%BCrth-elektronik/742792662/732-4670-1-ND/4310442" V 1850 1400 50  0001 C CNN "Digikey"
F 5 "0,14" V 1850 1400 50  0001 C CNN "Cost"
F 6 "Ignore" H 1850 1400 50  0001 C CNN "PN"
	1    1850 1400
	0    1    1    0   
$EndComp
$Comp
L obc_board-rescue:Ferrite_Bead_Small-Device FB17
U 1 1 6038341A
P 3200 1450
F 0 "FB17" H 3350 1450 50  0000 C CNN
F 1 "1k/600" V 3350 1600 50  0001 C CNN
F 2 "Inductor_SMD:L_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 3130 1450 50  0001 C CNN
F 3 "https://www.we-online.de/katalog/datasheet/742792662.pdf" H 3200 1450 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/w%C3%BCrth-elektronik/742792662/732-4670-1-ND/4310442" V 3200 1450 50  0001 C CNN "Digikey"
F 5 "0,14" V 3200 1450 50  0001 C CNN "Cost"
F 6 "Ignore" H 3200 1450 50  0001 C CNN "PN"
	1    3200 1450
	1    0    0    -1  
$EndComp
$Comp
L obc_board-rescue:Ferrite_Bead_Small-Device FB16
U 1 1 60383423
P 2800 1450
F 0 "FB16" H 2650 1450 50  0000 C CNN
F 1 "1k/600" V 2950 1600 50  0001 C CNN
F 2 "Inductor_SMD:L_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 2730 1450 50  0001 C CNN
F 3 "https://www.we-online.de/katalog/datasheet/742792662.pdf" H 2800 1450 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/w%C3%BCrth-elektronik/742792662/732-4670-1-ND/4310442" V 2800 1450 50  0001 C CNN "Digikey"
F 5 "0,14" V 2800 1450 50  0001 C CNN "Cost"
F 6 "Ignore" H 2800 1450 50  0001 C CNN "PN"
	1    2800 1450
	1    0    0    -1  
$EndComp
Connection ~ 2800 1550
$Comp
L obc_board-rescue:C_Small-Device C6
U 1 1 6038342D
P 1850 1550
F 0 "C6" V 1900 1600 50  0000 L CNN
F 1 "100n" V 1900 1300 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 1850 1550 50  0001 C CNN
F 3 "https://www.yageo.com/upload/media/product/productsearch/datasheet/mlcc/UPY-GPHC_X7R_6.3V-to-50V_18.pdf" H 1850 1550 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/yageo/CC0603KPX7R7BB104/311-1335-1-ND/2103119" V 1850 1550 50  0001 C CNN "Digikey"
F 5 "0,08" V 1850 1550 50  0001 C CNN "Cost"
F 6 "C" H 1850 1550 50  0001 C CNN "PN"
	1    1850 1550
	0    -1   -1   0   
$EndComp
$Comp
L obc_board-rescue:C_Small-Device C7
U 1 1 60383436
P 1100 1700
F 0 "C7" V 1150 1750 50  0000 L CNN
F 1 "100n" V 1150 1450 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 1100 1700 50  0001 C CNN
F 3 "https://www.yageo.com/upload/media/product/productsearch/datasheet/mlcc/UPY-GPHC_X7R_6.3V-to-50V_18.pdf" H 1100 1700 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/yageo/CC0603KPX7R7BB104/311-1335-1-ND/2103119" V 1100 1700 50  0001 C CNN "Digikey"
F 5 "0,08" V 1100 1700 50  0001 C CNN "Cost"
F 6 "C" H 1100 1700 50  0001 C CNN "PN"
	1    1100 1700
	0    -1   -1   0   
$EndComp
$Comp
L obc_board-rescue:C_Small-Device C8
U 1 1 6038343F
P 1850 1850
F 0 "C8" V 1900 1900 50  0000 L CNN
F 1 "100n" V 1900 1600 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 1850 1850 50  0001 C CNN
F 3 "https://www.yageo.com/upload/media/product/productsearch/datasheet/mlcc/UPY-GPHC_X7R_6.3V-to-50V_18.pdf" H 1850 1850 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/yageo/CC0603KPX7R7BB104/311-1335-1-ND/2103119" V 1850 1850 50  0001 C CNN "Digikey"
F 5 "0,08" V 1850 1850 50  0001 C CNN "Cost"
F 6 "C" H 1850 1850 50  0001 C CNN "PN"
	1    1850 1850
	0    -1   -1   0   
$EndComp
$Comp
L obc_board-rescue:C_Small-Device C9
U 1 1 60383448
P 1100 1550
F 0 "C9" V 1150 1600 50  0000 L CNN
F 1 "100n" V 1150 1300 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 1100 1550 50  0001 C CNN
F 3 "https://www.yageo.com/upload/media/product/productsearch/datasheet/mlcc/UPY-GPHC_X7R_6.3V-to-50V_18.pdf" H 1100 1550 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/yageo/CC0603KPX7R7BB104/311-1335-1-ND/2103119" V 1100 1550 50  0001 C CNN "Digikey"
F 5 "0,08" V 1100 1550 50  0001 C CNN "Cost"
F 6 "C" H 1100 1550 50  0001 C CNN "PN"
	1    1100 1550
	0    -1   -1   0   
$EndComp
$Comp
L obc_board-rescue:C_Small-Device C12
U 1 1 60383451
P 1850 1250
F 0 "C12" V 1900 1300 50  0000 L CNN
F 1 "100n" V 1900 1000 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 1850 1250 50  0001 C CNN
F 3 "https://www.yageo.com/upload/media/product/productsearch/datasheet/mlcc/UPY-GPHC_X7R_6.3V-to-50V_18.pdf" H 1850 1250 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/yageo/CC0603KPX7R7BB104/311-1335-1-ND/2103119" V 1850 1250 50  0001 C CNN "Digikey"
F 5 "0,08" V 1850 1250 50  0001 C CNN "Cost"
F 6 "C" H 1850 1250 50  0001 C CNN "PN"
	1    1850 1250
	0    -1   -1   0   
$EndComp
$Comp
L obc_board-rescue:C_Small-Device C13
U 1 1 6038345A
P 3100 1550
F 0 "C13" V 3100 1300 50  0000 L CNN
F 1 "100n" V 3100 1050 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 3100 1550 50  0001 C CNN
F 3 "https://www.yageo.com/upload/media/product/productsearch/datasheet/mlcc/UPY-GPHC_X7R_6.3V-to-50V_18.pdf" H 3100 1550 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/yageo/CC0603KPX7R7BB104/311-1335-1-ND/2103119" V 3100 1550 50  0001 C CNN "Digikey"
F 5 "0,08" V 3100 1550 50  0001 C CNN "Cost"
F 6 "C" H 3100 1550 50  0001 C CNN "PN"
	1    3100 1550
	0    -1   -1   0   
$EndComp
$Comp
L obc_board-rescue:C_Small-Device C5
U 1 1 60383463
P 1850 850
F 0 "C5" V 1800 900 50  0000 L CNN
F 1 "10u" V 1800 650 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 1850 850 50  0001 C CNN
F 3 "http://datasheets.avx.com/cx5r.pdf" H 1850 850 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/avx-corporation/0603ZD106KAT2A/478-10766-1-ND/7536554" V 1850 850 50  0001 C CNN "Digikey"
F 5 "0,1" V 1850 850 50  0001 C CNN "Cost"
F 6 "C" H 1850 850 50  0001 C CNN "PN"
	1    1850 850 
	0    -1   -1   0   
$EndComp
$Comp
L obc_board-rescue:C_Small-Device C10
U 1 1 6038346C
P 3100 1700
F 0 "C10" V 3100 1800 50  0000 L CNN
F 1 "1u" V 3100 2000 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 3100 1700 50  0001 C CNN
F 3 "http://weblib.samsungsem.com/mlcc/mlcc-ec-data-sheet.do?partNumber=CL10A105KP8NNN" H 3100 1700 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/samsung-electro-mechanics/CL10A105KP8NNNC/1276-1182-1-ND/3889268" V 3100 1700 50  0001 C CNN "Digikey"
F 5 "0,08" V 3100 1700 50  0001 C CNN "Cost"
F 6 "C" H 3100 1700 50  0001 C CNN "PN"
	1    3100 1700
	0    1    1    0   
$EndComp
Wire Wire Line
	3200 1550 3200 1700
Connection ~ 3200 1700
Wire Wire Line
	3200 1700 3200 2000
Connection ~ 3000 1700
Text GLabel 1100 7600 0    50   Input ~ 0
GND
Text GLabel 1700 7600 2    50   Input ~ 0
AGND
Wire Wire Line
	1550 7600 1700 7600
Text GLabel 3250 3000 2    50   Input ~ 0
GPIO_01
Text GLabel 3250 3100 2    50   Input ~ 0
USB_OTG_FS_SOF
Text GLabel 3250 3200 2    50   Input ~ 0
USB_OTG_FS_VBUS
Text GLabel 3250 3300 2    50   Input ~ 0
USB_OTG_FS_ID
Text GLabel 3250 3400 2    50   Input ~ 0
USB_OTG_FS_DM
Text GLabel 3250 3500 2    50   Input ~ 0
USB_OTG_FS_DP
Text GLabel 3250 6600 2    50   Input ~ 0
USB_OTG_FS_NOE
Text GLabel 3250 2600 2    50   Input ~ 0
TIM_2_CH_4
Text GLabel 3250 2800 2    50   Input ~ 0
TIM_2_CH_1
$Comp
L obc_board-rescue:SolderJumper_2_Open-Jumper JP9
U 1 1 60383483
P 3350 850
F 0 "JP9" H 3350 1055 50  0000 C CNN
F 1 "SolderJumper_2_Open" H 3350 964 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_TrianglePad1.0x1.5mm" H 3350 850 50  0001 C CNN
F 3 "~" H 3350 850 50  0001 C CNN
F 4 "Ignore" H 3350 850 50  0001 C CNN "PN"
	1    3350 850 
	1    0    0    -1  
$EndComp
Wire Wire Line
	3500 850  3600 850 
Wire Wire Line
	3200 850  3100 850 
Text GLabel 3100 850  0    50   Input ~ 0
VDD_UC
Text GLabel 3600 850  2    50   Input ~ 0
VDD
$Comp
L obc_board-rescue:SolderJumper_2_Open-Jumper JP45
U 1 1 6038348E
P 1400 7600
F 0 "JP45" H 1400 7805 50  0000 C CNN
F 1 "SolderJumper_2_Open" H 1400 7714 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_TrianglePad1.0x1.5mm" H 1400 7600 50  0001 C CNN
F 3 "~" H 1400 7600 50  0001 C CNN
F 4 "Ignore" H 1400 7600 50  0001 C CNN "PN"
	1    1400 7600
	1    0    0    -1  
$EndComp
Wire Wire Line
	1100 7600 1250 7600
$Comp
L obc_board-rescue:Ferrite_Bead_Small-Device FB7
U 1 1 60383498
P 1200 1250
F 0 "FB7" V 1250 1350 50  0000 C CNN
F 1 "1k/600" V 1350 1400 50  0001 C CNN
F 2 "Inductor_SMD:L_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 1130 1250 50  0001 C CNN
F 3 "https://www.we-online.de/katalog/datasheet/742792662.pdf" H 1200 1250 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/w%C3%BCrth-elektronik/742792662/732-4670-1-ND/4310442" V 1200 1250 50  0001 C CNN "Digikey"
F 5 "0,14" V 1200 1250 50  0001 C CNN "Cost"
F 6 "Ignore" H 1200 1250 50  0001 C CNN "PN"
	1    1200 1250
	-1   0    0    1   
$EndComp
Wire Wire Line
	1200 2000 2050 2000
Wire Wire Line
	1200 2000 1200 1850
Connection ~ 1200 2000
Connection ~ 1200 1700
Wire Wire Line
	1200 1700 1200 1550
Connection ~ 1200 1850
Wire Wire Line
	1200 1850 1200 1700
Wire Wire Line
	1000 2000 1000 1850
Connection ~ 1000 1700
Wire Wire Line
	1000 1700 1000 1550
Connection ~ 1000 1850
Wire Wire Line
	1000 1850 1000 1700
Wire Wire Line
	1000 1550 1000 1350
Connection ~ 1000 1550
Wire Wire Line
	1200 1550 1200 1350
Connection ~ 1200 1550
Text GLabel 1200 1150 1    50   Input ~ 0
VDD_UC
Wire Wire Line
	2250 2000 2250 1850
Text GLabel 1700 1700 0    50   Input ~ 0
VDD_UC
Text GLabel 1700 1850 0    50   Input ~ 0
GND
Wire Wire Line
	1700 1700 1750 1700
Wire Wire Line
	1700 1850 1750 1850
Wire Wire Line
	1950 1700 1950 1850
Wire Wire Line
	1950 1850 2250 1850
Connection ~ 1950 1850
Wire Wire Line
	2550 2000 2550 1950
Wire Wire Line
	2550 1950 2150 1950
Wire Wire Line
	2150 1950 2150 2000
Connection ~ 2150 2000
Text GLabel 1700 1400 0    50   Input ~ 0
VDD_UC
Text GLabel 1700 1550 0    50   Input ~ 0
GND
Wire Wire Line
	1700 1400 1750 1400
Wire Wire Line
	1700 1550 1750 1550
Text GLabel 1700 1100 0    50   Input ~ 0
VDD_UC
Text GLabel 1700 1250 0    50   Input ~ 0
GND
Wire Wire Line
	1700 1100 1750 1100
Wire Wire Line
	1700 1250 1750 1250
Wire Wire Line
	1950 1400 1950 1550
Wire Wire Line
	1950 1550 2350 1550
Wire Wire Line
	2350 1550 2350 2000
Connection ~ 1950 1550
Wire Wire Line
	1950 1100 1950 1250
Wire Wire Line
	1950 1250 2450 1250
Wire Wire Line
	2450 1250 2450 2000
Connection ~ 1950 1250
Wire Wire Line
	1700 850  1750 850 
Wire Wire Line
	1950 850  2000 850 
Text GLabel 3250 4300 2    50   Input ~ 0
SPI_1_CS_E_0
Text GLabel 1450 6400 0    50   Input ~ 0
SPI_1_CS_E_1
$Comp
L obc_board-rescue:C_Small-Device C22
U 1 1 603B6A49
P 5300 2650
F 0 "C22" H 5400 2600 50  0000 L CNN
F 1 "20p" H 5400 2700 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 5300 2650 50  0001 C CNN
F 3 "https://www.yageo.com/upload/media/product/productsearch/datasheet/mlcc/UPY-GP_NP0_16V-to-50V_18.pdf" H 5300 2650 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/yageo/CC0603GRNPO9BN200/311-3924-1-ND/8025013" H 5300 2650 50  0001 C CNN "Digikey"
F 5 "0,13" H 5300 2650 50  0001 C CNN "Cost"
F 6 "C" H 5300 2650 50  0001 C CNN "PN"
	1    5300 2650
	-1   0    0    1   
$EndComp
Text GLabel 5650 4700 1    50   Input ~ 0
RESET
Text GLabel 6050 4700 1    50   Input ~ 0
GND
$Comp
L obc_board-rescue:SW_Push-Switch SW1
U 1 1 603B6A54
P 5850 4700
F 0 "SW1" H 5850 4850 50  0000 C CNN
F 1 "SW_Push" H 5900 4950 50  0000 C CNN
F 2 "Button_Switch_SMD:SW_SPST_PTS645" H 5850 4900 50  0001 C CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1977263&DocType=Customer+Drawing&DocLang=English" H 5850 4900 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/te-connectivity-alcoswitch-switches/FSM1LPSTR/450-2156-1-ND/5343836" H 5850 4700 50  0001 C CNN "Digikey"
F 5 "0,38" H 5850 4700 50  0001 C CNN "Cost"
F 6 "X" H 5850 4700 50  0001 C CNN "PN"
	1    5850 4700
	1    0    0    -1  
$EndComp
Text Notes 5000 1850 0    50   ~ 0
Oscilator
Wire Wire Line
	6050 4850 6050 4700
Wire Wire Line
	5950 4850 6050 4850
Wire Wire Line
	5750 4850 5650 4850
Wire Wire Line
	5650 4850 5650 4700
Text Notes 5600 4400 0    50   ~ 0
Reset Switch
Wire Notes Line
	5550 4300 6150 4300
Wire Notes Line
	6150 4300 6150 4950
Wire Notes Line
	6150 4950 5550 4950
Wire Notes Line
	5550 4950 5550 4300
Text GLabel 6900 2750 1    50   Input ~ 0
GND
Text Notes 5000 3500 0    50   ~ 0
C L = C L1 x C L2 / (C L1 + C L2 ) + C stray\nc_stray =10 pf\nC_L = 20 pf (oder etwas weniger, je nach Quarz)\nC_L_1 = C_L_2 = C_A\nC_L = C_A * CA / (2* C_A ) + C stray\n20 = C_A*C_A/(2*C_A) + C_Stray\n10 = C_A*C_A/(2*C_A) => C_A = 20 pF\n\n
$Comp
L obc_board-rescue:C_Small-Device C14
U 1 1 603B6A6E
P 5850 4850
F 0 "C14" V 5900 4900 50  0000 L CNN
F 1 "100n" V 5900 4600 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 5850 4850 50  0001 C CNN
F 3 "https://www.yageo.com/upload/media/product/productsearch/datasheet/mlcc/UPY-GPHC_X7R_6.3V-to-50V_18.pdf" H 5850 4850 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/yageo/CC0603KPX7R7BB104/311-1335-1-ND/2103119" V 5850 4850 50  0001 C CNN "Digikey"
F 5 "0,08" V 5850 4850 50  0001 C CNN "Cost"
F 6 "C" H 5850 4850 50  0001 C CNN "PN"
	1    5850 4850
	0    1    1    0   
$EndComp
$Comp
L obc_board-rescue:C_Small-Device C23
U 1 1 603B6A77
P 6200 2650
F 0 "C23" H 5950 2600 50  0000 L CNN
F 1 "20p" H 5950 2700 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 6200 2650 50  0001 C CNN
F 3 "https://www.yageo.com/upload/media/product/productsearch/datasheet/mlcc/UPY-GP_NP0_16V-to-50V_18.pdf" H 6200 2650 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/yageo/CC0603GRNPO9BN200/311-3924-1-ND/8025013" H 6200 2650 50  0001 C CNN "Digikey"
F 5 "0,13" H 6200 2650 50  0001 C CNN "Cost"
F 6 "C" H 6200 2650 50  0001 C CNN "PN"
	1    6200 2650
	-1   0    0    1   
$EndComp
Text GLabel 5300 2400 1    50   Input ~ 0
RCC_OSC_IN
Text GLabel 6250 2400 2    50   Input ~ 0
RCC_OSC_OUT
Connection ~ 3200 1550
$Comp
L obc_board-rescue:ASCO-Oscillator Y1
U 1 1 602F74FE
P 5750 2400
F 0 "Y1" H 5500 2700 50  0000 L CNN
F 1 "ASEMB 24M/20p" H 5850 2700 50  0000 L CNN
F 2 "Oscillator:Oscillator_SMD_Abracon_ASE-4Pin_3.2x2.5mm_HandSoldering" H 5850 2050 50  0001 C CNN
F 3 "https://abracon.com/Oscillators/ASEMB.pdf" H 5525 2525 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/abracon-llc/ASEMB-24-000MHZ-XY-T/535-11123-6-ND/2624795" H 5750 2400 50  0001 C CNN "Digikey"
F 5 "2,39" H 5750 2400 50  0001 C CNN "Cost"
	1    5750 2400
	1    0    0    -1  
$EndComp
Text GLabel 5750 2000 1    50   Input ~ 0
VDD
Wire Wire Line
	5750 2000 5750 2050
Wire Wire Line
	5300 2550 5300 2400
Wire Wire Line
	5300 2400 5350 2400
Wire Wire Line
	5300 2750 5750 2750
Wire Wire Line
	5750 2750 5750 2700
Connection ~ 5750 2750
Wire Wire Line
	6200 2750 5750 2750
Wire Wire Line
	6200 2550 6200 2400
Wire Wire Line
	6200 2400 6150 2400
Wire Wire Line
	6250 2400 6200 2400
Connection ~ 6200 2400
Wire Wire Line
	6200 2750 7100 2750
Connection ~ 6200 2750
Wire Wire Line
	7100 2550 7100 2050
Wire Wire Line
	7100 2050 5750 2050
Connection ~ 5750 2050
Wire Wire Line
	5750 2050 5750 2100
Wire Notes Line
	7450 3450 7450 1750
Wire Notes Line
	7450 1750 4950 1750
Wire Notes Line
	4950 1750 4950 3450
Wire Notes Line
	4950 3450 7450 3450
$Comp
L obc_board-rescue:C_Small-Device C31
U 1 1 6035C681
P 7100 2650
F 0 "C31" H 7200 2700 50  0000 L CNN
F 1 "10n" H 7200 2600 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 7100 2650 50  0001 C CNN
F 3 "https://www.yageo.com/upload/media/product/productsearch/datasheet/mlcc/UPY-GPHC_X7R_6.3V-to-50V_18.pdf" H 7100 2650 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/yageo/CC0603KPX7R9BB103/311-1572-1-ND/4340579" V 7100 2650 50  0001 C CNN "Digikey"
F 5 "0,08" V 7100 2650 50  0001 C CNN "Cost"
F 6 "C" H 7100 2650 50  0001 C CNN "PN"
	1    7100 2650
	1    0    0    -1  
$EndComp
$EndSCHEMATC
