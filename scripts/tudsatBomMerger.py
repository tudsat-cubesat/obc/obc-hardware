#
# TUDSaT Bill of material merger
# USAGE python3 thisScript <bom.csv> <parts.csv>
#


import csv
import sys
import time

pnString = "PN"
valueString = "Value"
footprintString = "Footprint"
dkRefString = "Digikey_Ref"

ignoreString = "Ignore"


matchingFields = ["PN","Value","Footprint"]# not used yet

knownFootprints = ["0603","0805","SOT-24"] # not used yet & unsatisfing solution

def find_nth(haystack, needle, n):
    start = haystack.find(needle)
    while start >= 0 and n > 1:
        start = haystack.find(needle, start+len(needle))
        n -= 1
    return start


def getMapedFootprint(entry):
	for fp in knownFootprints:
		if fp.lower() in entry.lower():
			return fp.lower()

	return "UNKNOWN_FOOTPRINT"

def getIndex(list, name):
	i = 0
	for column in list:
		if column.strip() == name:
			return i
		i = i + 1

	return -1

def getPartIdentifier(pn,value,table,pn_index,value_index,dkRef_index):
	for line in partTable:
		if line[pn_index] == pn and line[value_index] == value:
			return line[dkRef_index]
	return "UNKNOWN COMPONENT"	


try:
	ts = time.gmtime()
	target = open("tudsatBOM" + time.strftime("%Y-%m-%d_%H:%M:%S", ts) + ".csv", 'w')

	bom = open(sys.argv[1], 'r')
	parts = open(sys.argv[2], 'r')
	print(parts)
	#exit()
except IOError:
	e = "Can't open output file for writing. " 
	print(__file__, ":", e, sys.stderr)
	f = sys.stdout


#Indexes of field that will be used for matching
pn_index_bom = 0
pn_index_parts = 0

value_index_bom = 0
value_index_parts = 0

footprint_index_bom = 0
footprint_index_parts = 0

#Opening files ...
bomReader = csv.reader(bom, lineterminator='\n', delimiter=',', quotechar='\"', quoting=csv.QUOTE_ALL)
partReader = csv.reader(parts, lineterminator='\n', delimiter=',', quotechar='\"', quoting=csv.QUOTE_ALL)
targetWriter = csv.writer(target, lineterminator='\n', delimiter=',', quotechar='\"', quoting=csv.QUOTE_ALL)

headerBom = bomReader.__next__()
headerParts = partReader.__next__()


name_index_bom  = getIndex(headerBom,"Ref")
pn_index_bom  = getIndex(headerBom,pnString)
value_index_bom  = getIndex(headerBom,valueString)
footprint_index_bom  = getIndex(headerBom,footprintString)

pn_index_parts  = getIndex(headerParts,pnString)
value_index_parts  = getIndex(headerParts,valueString)
footprint_index_parts  = getIndex(headerParts,footprintString)

dkRef_index_parts  = getIndex(headerParts,dkRefString)


#Extract parts from csv file

partTable = []

for line in partReader:
	partTable.append(line)


headerBom.append("DK_Ref")


targetWriter.writerow(headerBom)


for contentRow in bomReader:
	s = contentRow[name_index_bom]
	pn = contentRow[pn_index_bom]
	if pn != ignoreString:
		contentRow.append(
			getPartIdentifier(pn,
				contentRow[value_index_bom],
				partTable,
				pn_index_parts,
				value_index_parts,
				dkRef_index_parts ))
		targetWriter.writerow(contentRow)