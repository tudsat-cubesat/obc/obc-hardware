#! / bin/bash
# please provide paths to the kicad_pcb and xml files
pcb=../obc_board/obc_board.kicad_pcb
xml=../obc_board/obc_board.xml
csv=../obc_board/obc_board.csv
parts=./tudsatParts_2020_10_05.csv
rm -r ../autoBOM
mkdir ../autoBOM
python2 ./InteractiveHtmlBom/generate_interactive_bom.py $pcb --dest-dir ../autoBOM/
python3 ./tudsatBomMerger.py $csv $parts
cp ./tudsatParts*.csv ../autoBOM
mv ./tudsatBOM*.csv ../autoBOM
